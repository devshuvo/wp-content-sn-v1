<?php
/**
 * Template Name: Give 5 Landing
 *
 * @package Splendid
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class('m0'); ?>>

		<?php astra_primary_content_top(); ?>
		<?php astra_content_page_loop(); ?>

		<div class="give5-header-outer">
			<div class="give5-header-bg">
				<h1>Give $5, Get $5</h1>
				<p>Love Splendid? Get rewards for sharing!</p>
			</div>
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-title-shape.png" class="sep">

			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-left.svg" class="give5-left  hide-on-phone">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-right.svg" class="give5-right hide-on-phone">
		</div>

		<?php splendid_give_5_love_area('give5'); ?>

		<div class="give5-bottom-outer">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-black-shape.png" class="sep">
			<div class="give5-bottom-wrap">
				<div class="give5-container">
					<div class="give5-half-right">
						<div class="give5-bottom-text">
							<?php echo do_shortcode("[hfe_template id='495']"); ?>
						</div>
					</div>
				</div>
			</div>
		</div>


		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>