<?php
/**
 * Refer a friend content
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( !is_user_logged_in() ) {
	return;
}

//$affiliate = YITH_WCAF_Affiliate_Handler()->get_affiliate_by_id( 9 );

do_action( 'woocommerce_before_account_refer_friend' ); ?>

<div class="sn-referral-content">

	<div class="sn-ref-title">
		<h3>Refer-A-Friend</h3>

	</div>

	<div class="refer-text text-lg">
		<p>Send a friend $5, and get a free box when 3 purchase, and get $5 cash credit for each purchase after that!</p>
	</div>

	<div class="refer-text text-lg">
		<p>You can share your unique link with friends using any of the options below:</p>
	</div>

	<div class="snref-progress text-center">
		<div class="snref-progress-text text-lg"><?php echo sn_more_to_go_count_with_text(); ?></div>
		<ul>

			<?php echo sn_referrer_progress_count("1"); ?>
			<?php echo sn_referrer_progress_count("2"); ?>
			<?php echo sn_referrer_progress_count("3"); ?>
		</ul>
	</div>

	<div class="snref-link-generator">
		<?php echo do_shortcode( "[yith_wcaf_link_generator]" ); ?>
	</div>

</div>

<?php do_action( 'woocommerce_after_account_refer_friend' ); ?>
