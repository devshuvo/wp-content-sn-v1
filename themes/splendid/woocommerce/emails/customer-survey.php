<?php
/**
 * Survey email
 *
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>



<p><?php printf( esc_html__( 'Who Submitted?: %1$s.', 'splendid' ), esc_html( $formdata['referrer_email'] ) ); ?></p>
<p></p>

<p><?php printf( esc_html__( 'Did you like Splendid Focus overall? How would you rate the product?: %1$s.', 'splendid' ), esc_html( $formdata['product_rating'] ) ); ?></p>
<p></p>

<p><?php printf( esc_html__( 'How did the product work for you? Did it it have an effect on your level of focus, mood, or mental energy?: %1$s.', 'splendid' ), esc_html( $formdata['level_of_focus'] ) ); ?></p>
<p><?php printf( esc_html__( 'Additional comment: %1$s.', 'splendid' ), esc_html( $formdata['level_of_focus_comment'] ) ); ?></p>
<p></p>


<p><?php printf( esc_html__( 'How did it taste?: %1$s.', 'splendid' ), esc_html( $formdata['taste'] ) ); ?></p>
<p><?php printf( esc_html__( 'Additional comment: %1$s.', 'splendid' ), esc_html( $formdata['taste_comment'] ) ); ?></p>
<p></p>

<p><?php printf( esc_html__( 'Would you recommend Splendid Focus to a friend?: %1$s.', 'splendid' ), esc_html( $formdata['recommend_friend'] ) ); ?></p>
<p></p>

<p><?php echo esc_html__( 'Please leave a brief review of 100 characters or more: %1$s.', 'splendid' ); ?></p>
<p><?php esc_html_e( $formdata['taste_comment'] ); ?></p>
<p></p>


<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

do_action( 'woocommerce_email_footer', $email );
