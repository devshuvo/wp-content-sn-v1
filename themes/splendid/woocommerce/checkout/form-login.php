<?php
/**
 * Checkout login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

if ( is_user_logged_in() || 'no' === get_option( 'woocommerce_enable_checkout_login_reminder' ) ) {
	return;
}

?>

<div class="sn-checkout-login-form show_on_ci">
	<div class="sn-checkout-login-header">
		<div class="contact-info">Contact Information</div>
		<div class="have-acc">Have an account? <a class="sign-in-toggle fapb showlogin" href="#">Sign in</a></div>
	</div>

	<?php
	woocommerce_login_form(
		array(
			'message' => '<div class="text-sm message">Sign in to an account</div>',
			'login_text' => 'Sign In',
			'redirect' => wc_get_checkout_url()."?loggedin=true",
			'hidden'   => 1,
		)
	);
	?>
	<div class="sn_checkout_signup_wrap">
		<form id="sn_checkout_signup" class="sn_checkout_signup" method="post">
			<p class="status"></p>
			<div class="sn-form-row sn_validation-required">
		        <input class="input-text" type="email" name="email" id="sn_billing_email" placeholder="Email" autocomplete="email" required>
		    </div>

		    <div class="sn_createaccount-wrap">
		    	<input type="checkbox" name="sn_createaccount" id="sn_createaccount" value="1" style="display: none;">
		    	<div class="sn_checkbox-icon">
		    		<?php echo splendid_svg('checkbox'); ?>
		    	</div>
		    	<label class="gray fapr" for="sn_createaccount">Create an account for faster checkout</label>
		    </div>

	        <div class="create-skip-pass" style="display: none;">
	        	<div class="form-row-create-skip">
		            <button type="submit">Create</button>
		            <span class="skip_from_signup">Skip</span>
		        </div>
	            <div class="form-row-pass">
	            	<input class="input-text" id="password" type="password" name="password" placeholder="Password" autocomplete="new-password" required>
	            </div>
	        </div>
	        <input type="hidden" name="redirect_to" value="<?php echo wc_get_checkout_url(); ?>">
	        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	    </form>
	</div>

	<div class="sn_checkout-newsletter-wrap">
		<input type="checkbox" name="sn_checkout-newsletter" id="sn_checkout-newsletter" value="1" style="display: none;" checked="checked">
		<div class="sn_checkbox-icon">
			<?php echo splendid_svg('checkbox'); ?>
		</div>
		<label class="gray fapr" for="sn_checkout-newsletter">Yes, I would like to receive special offers and news via email.</label>
	</div>
</div>
