// Get Cookie function
function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}

// Start Jquery
(function($){
	'use strict'
	$(document).ready(function($){
		console.log('hello from theme');

		$(document).on('click', '.direct_checkout_btn, .direct_cart_btn', function(e){

			var qty = $(this).closest('form').find('input.qty').val();

			$(this).attr("href", function() {
				return this.href + '&quantity=' + qty;
			});


			//e.preventDefault();
		});

		// Direct cart button
		$(document).on('click', '.direct_cart_btn', function(e){
			$(this).closest('form').find('button[type="submit"]').click();
		});

		// Share Icon click
		$(document).on('click', '.snref-share-wrap .snref-share-svg', function(){

			console.log('clicked');
			$(this).closest('li').find('#share_link_button').click();

		});

		// Popup Close overlay
		$(document).on('click', '.close-popup-overlay', function(){


			var popUpId = $(this).attr('data-id');

			$('#'+popUpId).find('.trigger_close_btn').click();

		});


		// TOC
		$(document).on('mouseover', '.sn-sticky-toc .elementor-toc__list-item-text', function(e){

			// target element id
    		var id = $(this).attr('href');
    		if ( id ) {
	    		$(this).attr('href','');
	    		$(this).attr('data-href', id);
	    	}

		});

		$(document).on('click', '.sn-sticky-toc .elementor-toc__list-item-text', function(e){

			e.preventDefault();

			// target element id
    		var id = $(this).attr('href');
    		if ( id ) {
	    		$(this).attr('href','');
	    		$(this).attr('data-href', id);
	    	}

    		var data_id = $(this).attr('data-href');

		    // target element
		    var $data_id = $(data_id);
		    if ($data_id.length === 0) {
		        return;
		    }

			// top position relative to the document
		    var pos = $data_id.offset().top-160;

		    // animated top scrolling
		    $('body, html').animate({scrollTop: pos});

		    console.log(id);

			return false;

		});

		// Skip Create an account on Checkout
		$(document).on('click', '.skip_from_signup', function(e){

			$(document).find('label[for="sn_createaccount"]').click();

		});

		// Custom Radio Click to trigger
		$(document).on('click', '.sn_checkbox-icon, .sn_radio-icon', function(e){

			//$(this).parent().find('label').click();
			$(this).siblings('label').click();

			if ( $(this).parent().hasClass('wc_payment_method') ) {
				$(this).siblings('.input-radio').click().prop( 'checked', true );
			}

		});

		// WC comment button
		$(document).on('click', '.add-review-btn', function(e){
			e.preventDefault();
			$("#review_form_wrapper").slideToggle();
		});

		$(document).on('click', '.trigger_back_btn, .trigger_close_btn', function(e){
			e.preventDefault();

			$(this).closest('.sn_popup_container, .sns-popup').fadeOut();

			var openedPopup = $('html').attr('data-opened-popup');

			if ( openedPopup > 0 ) {
				openedPopup = parseInt(openedPopup)-1;
			} else {
				//openedPopup = 0;
			}

			$('html').attr('data-opened-popup', openedPopup);

			if ( openedPopup < 1 ) {
				$('html').removeClass('popup-active');
			}

			var openedPopupId = $(this).closest('.sn_popup_container, .sns-popup').attr('id');

			$('.modal_show').each(function(i, el){
				if( $(this).attr('data-modal_id') == openedPopupId ){
					$(this).removeClass('modal_show');
				}
			});

		});


		$(document).on('click', '[data-modal_id]:not(.modal_show)', function(e){
			e.preventDefault();

			$(this).addClass('modal_show');

			var modal_id = $(this).data('modal_id');

			$(document).find('#'+modal_id).fadeIn();

			var openedPopup = $('html').attr('data-opened-popup');

			if ( openedPopup ) {
				openedPopup = parseInt(openedPopup)+1;
			} else {
				//openedPopup = 0;
			}

			$('html').addClass('popup-active').attr('data-opened-popup', openedPopup);

		});

		// form toggle
		$(document).on('click', '[data-toggle-show]', function(e){
			var tId = $(this).data('toggle-show');

			$(this).closest('[data-toggle-hide]').hide();

			$(tId).show();

		});

		// Purchase Type Changing
		$(document).on('change', '.radio-container input[name="purchase_type"]', function(e){
			var tVal = $(this).val();

			if (tVal == 1) {
				$(document).find('.show_on_sns').addClass('show');
				$(document).find('.show_on_otp').removeClass('show');
			} else {
				$(document).find('.show_on_sns').removeClass('show');
				$(document).find('.show_on_otp').addClass('show');
			}

		});

		// Price Change on qty change
		jQuery(document).on('change', 'input[name="quantity"]', function(e){
			var $this = jQuery(this);
			var qty = $this.val();

			// .snpopup-price
			jQuery('.sn-product-price .sn_wc_price, .snpopup-price .sn_wc_price').each(function(el,i ){
				var thePrice = parseFloat( jQuery(this).data('price') );

				var returnPrice = (thePrice*qty).toFixed(2);

				jQuery(this).text( returnPrice );

			});

		});

		// Different shippiing address Changing
		$(document).on('change', '.sn_different_shipping', function(e){
			var tVal = $(this).val();

			if (tVal == 1) {
				jQuery('#ship-to-different-address-checkbox').prop('checked', 1).change();
			} else {
				jQuery('#ship-to-different-address-checkbox').prop('checked', 0).change();
			}

		});

		// Create Checkout Account Checkbox
		$(document).on('change', '#sn_createaccount', function(e){
			var tVal = $(this).is(':checked');

			if (tVal) {
				jQuery('.create-skip-pass').slideDown();
				jQuery('.create-account label.woocommerce-form__label').trigger('click');
			} else {
				jQuery('.create-skip-pass').slideUp();
				jQuery('.create-account label.woocommerce-form__label').trigger('click');
			}

		});

		// Checkbox add class
		$(document).on('change', '.sn_radio-wrap input[type="radio"]', function(e){
			var tVal = $(this).val();
			var tChecked = $(this).is(':checked');

			$('.sn_radio-wrap').removeClass('active');

			if (tChecked) {
				$(this).closest('.sn_radio-wrap').addClass('active');
				jQuery('#shipping_country').change();
			}

		});

		// Payment Checkbox add class
		$(document).on('change', '.wc_payment_method input[type="radio"]', function(e){
			var tVal = $(this).val();
			var tChecked = $(this).is(':checked');

			$('.wc_payment_method').removeClass('active');

			if (tChecked) {
				$(this).closest('.wc_payment_method').addClass('active');
			}

		});

		function sn_billing_validated(){
			var flag = 0;

			jQuery('.woocommerce-billing-fields .validate-required, .woocommerce-billing-fields .validate-postcode, .sn_validation-required').each(function(i,el){

				var inputVal = jQuery(this).find('input, select').val();

				if ( inputVal == "" || inputVal == "default" ) {
					jQuery(this).addClass('woocommerce-invalid');
					flag++;
				} else {
					jQuery(this).removeClass('woocommerce-invalid');
				}

			});


			if( flag > 0 ){
				return false;
			} else {
				return true;
			}
		}

		// Checkout Step Change - to payment
		$(document).on('click', '.process-to-payment, .step-payment', function(e) {
			e.preventDefault();

			if ( sn_billing_validated() ) {
				$('.show_on_ci').hide();
				$('.step-payment').removeClass('inactive');

				$('.show_on_pi').show();

				$(document).trigger('show_submitted_billing_info');

				$('.sn_notifications').html('');

			} else {
				$('.sn_notifications').html('<span class="sn-danger">Whoops! Please enter required fields.</span>');
			}

			// Change Billing Address/Payment order
        	jQuery('.woocommerce-shipping-fields-wrap').insertAfter('.woocommerce-checkout-review-order-wrap');

			$("html, body").animate({ scrollTop: 0 }, 300);
 		 	return false;

		});

		// Checkout Step Change - to customer info
		$(document).on('click', '.step-information, .edit-billing-info', function(e) {
			e.preventDefault();

			$('.show_on_ci').show();
			$('.step-payment').addClass('inactive');

			$('.show_on_pi').hide();

			$(document).trigger('show_submitted_billing_info');

			$("html, body").animate({ scrollTop: 0 }, 300);
 		 	return false;

		});
		// End Checkout Step Change

		// Amazon Pay Trigger
		$(document).on('click', '.sn-amazon-pay', function(e) {
			e.preventDefault();

			jQuery('#pay_with_amazon img').click();

		});


		// Show customer billing info
		//$(document).on('keyup', '.woocommerce-billing-fields__field-wrapper input', function(e){
		//	$(document).trigger('show_submitted_billing_info');
		//});

		$(document).on('show_submitted_billing_info', function() {
			var formData = $('form.checkout').serializeArray();

			formData.forEach(function(el, i){
				//console.log( el.name, el.value );

				var value = el.value;
				var element = el.name;

				if (value) {
					if ( element == "billing_city" || element == "billing_state" ) {
						$('.sn-'+element).text(value+',').show();
					} else {
						$('.sn-'+element).text(value).show();
					}

				} else {
					$('.sn-'+element).hide();
				}

			});

			// Subscribe & Save TOS
			$('.purchase_type_class').each(function(){
				if ( $(this).hasClass('subscribe-save') ) {
					$('.subscrive-save-tos').show();
				}
			});

		});
		// End Show customer billing info


		// Complete order form submit
		$(document).on('click', '.sn-complete-order', function() {
			$('button#place_order').trigger('click');
		});

		// Skip login form
		$(document).on('click', '.skip_from_login', function() {
			$(this).closest('.woocommerce-form-login').slideUp();
		});

		// Coupon Code Applying
		$(document).on('click', '#sn_coupon_submit', function() {
			$(this).closest('.sn-form-coupon').block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
			$('.checkout_coupon').submit();
		});

		$( document.body ).on('applied_coupon_in_checkout', function() {
			jQuery('.sn-form-coupon').unblock();
		});

		$(document).on('change keyup', '#sn_coupon_code', function() {
			var thisVal = $(this).val();
		    $('#coupon_code').val(thisVal);
		});

		$(document).on('change keyup', '#coupon_code', function() {
			var thisVal = $(this).val();
		    $('#sn_coupon_code').val(thisVal);
		});
		// End Coupon Code Applying

		// Email Sync for checkout
		$(document).on('change keyup', '#sn_billing_email', function() {
		    $('#billing_email').val($(this).val());
		    console.log('email change');
		});
		$(document).on('change keyup', '#billing_email', function() {
		    $('#sn_billing_email').val($(this).val());
		});
		jQuery("#billing_email").trigger('change');

		// Password Sync for checkout
		$(document).on('change keyup', '#sn_checkout_signup #password', function() {
		    $('#account_password').val($(this).val());
		});
		$(document).on('change keyup', '#account_password', function() {
		    $('#sn_checkout_signup #password').val($(this).val());
		});

		// Popup Cart button
		$(document).on('click', '.ssff-cart-popup-btn', function(e){
			e.preventDefault();

		    var $this = $(this);

		    var product_id = $(this).data('product_id');

		    var data = {
		        action: 'sn_product_detail_popup',
		        nonce: sn_ajax.nonce,
		        product_id: product_id,
		    }

		    $.ajax({
		      url: sn_ajax.url,
		      type: 'post',
		      data: data,
		      beforeSend : function ( xhr ) {
		        $this.addClass('disabled');
		        $('.sn_popup_container').find('.sn-product-wrap-outer').remove();
		        $('.sn_popup_container').fadeIn();
		        $('.splendid_logo_loader').fadeIn();


				var openedPopup = $('html').attr('data-opened-popup');
				if ( openedPopup ) {
					openedPopup = parseInt(openedPopup)+1;
				} else {
					//openedPopup = 0;
				}
				$('html').addClass('popup-active').attr('data-opened-popup', openedPopup);

		      },
		      success: function( res ) {
		      	$('.splendid_logo_loader').hide();
		      	$this.removeClass('disabled');

		      	// Data push
		      	$('.sn_popup_container').append(res);
		      },
		      error: function( result ) {
		      	//$('html').addClass('popup-active');
		        console.error( result );
		      }
		    });

		});

		// Perform AJAX login on form submit
        $(document).on('submit', 'form#login', function(e){
            e.preventDefault();
            var $this = $(this);

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: sn_ajax.url,
                data: {
                    'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                    'username': $('form#login #username').val(),
                    'password': $('form#login #password').val(),
                    'security': $('form#login #security').val()
                },
                success: function(data){
                    if ( data.loggedin == true ){
                        jQuery(document).trigger('ref_step_next');
                    } else {
                    	$this.find('.status').html( data.message );
                    }
                },
                beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			error: function(data){
    				console.log(data);
    			},
            });

        });

        // Register Form - Referrel
		$(document).on('submit', 'form#signup', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		// handle errors on survey page
    		if ( $('.survey-page').hasClass('survey-error-msg') ) {
    			if ( $('body').hasClass('survey_completed') ) {

    			} else {
    				$('body').addClass('survey_error');

    				$("html, body").animate({ scrollTop: 0 }, 300);
    				return;
    			}
    		}

    		var formData = new FormData(this);
            formData.append('action', 'sn_register_form');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('ref_step_next');
                    } else {
                    	$this.find('.status').html( response.error_log );
                    }

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

		// Register Form - Checkout
		$(document).on('submit', 'form#sn_checkout_signup', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);

            formData.append('newsletter_subscribe', jQuery('#sn_checkout-newsletter').is(':checked'));
            formData.append('action', 'sn_register_form__checkout');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('sn_register_form__checkout_success');
                    }

                    $this.find('.status').html( response.message );

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

        // Email Capture
		$(document).on('submit', '#splendid_email_capture', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);
            formData.append('action', 'splendid_email_capture');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });

                    $this.find('.notice').html("").hide();
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('ref_step_next');
                    }

                    $this.find('.notice').html( response.notice ).show();

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});


        // Survey Form Submit
		$(document).on('submit', '#customer_survey_form', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);
            formData.append('action', 'customer_survey_form');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });

                    $this.find('.notice').html("").hide();
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('ref_step_next');
                    }

                    $this.find('.notice').html( response.notice ).show();

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

		// Share copy
        $('button#share_link_button').click(function(){

            $(this).addClass('copied');
            setTimeout(function(){ $('button#share_link_button').removeClass('copied'); }, 3000);
            $(this).parent().find("#share_link_input").select();
            document.execCommand("copy");
        });

        // Pusle 15sec
        function animate_pulsing(){
        	$('.hero_shape_3').addClass('animating');
	        setTimeout(function(){
	        	$('.hero_shape_3').removeClass('animating');
	        },4000);
        }

        animate_pulsing();

        setInterval(function(){
        	animate_pulsing();
        },15000);



        $(window).load(function(){

        	// My reward count
        	$('.woocommerce-MyAccount-navigation-link--my-rewards a').append('<div class="count">'+sn_ajax.get_rewards+'</div>');

        	// Init popup count
        	$('html').attr('data-opened-popup', "0");

        	// State Trigger
        	jQuery('#billing_state').trigger('change');

        	// Auto Populate Referrer Email
        	$('form#signup').find('#sn_signup-email').val( getCookie('referrer_email') );
        	$('form#splendid_email_capture').find('#email').val( getCookie('referrer_email') );

        });


	});


	$(document).ready(function() {

		$('.woocommerce-product-gallery__image a').each(function(i,el){
			//console.log( $(this).html() );
			$('.woocommerce-product-thumb__wrapper').append('<div class="item-thumb">'+$(this).html()+'</div>');
		});
	});

	// OWL SYNC
	$(document).ready(function() {

	  	var sync1 = $("#sync1");
	  	var sync2 = $("#sync2");
	  	var slidesPerPage = 4; //globaly define number of elements per page
	  	var syncedSecondary = true;

	  	sync1.owlCarousel({
	  	  items : 1,
	  	  slideSpeed : 2000,
	  	  nav: true,
	  	  autoplay: 1,
	  	  dots: false,
	  	  loop: true,
	  	  responsiveRefreshRate : 200,
	  	  navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #999999;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #999999;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
	  	}).on('changed.owl.carousel', syncPosition);

	  	sync2
	  	  .on('initialized.owl.carousel', function () {
	  	    sync2.find(".owl-item").eq(0).addClass("current");
	  	  })
	  	  .owlCarousel({
	  	  items : slidesPerPage,
	  	  dots: false,
	  	  nav: false,
	  	  smartSpeed: 200,
	  	  slideSpeed : 500,
	  	  slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
	  	  responsiveRefreshRate : 100
	  	}).on('changed.owl.carousel', syncPosition2);

	  	function syncPosition(el) {
	  	  //if you set loop to false, you have to restore this next line
	  	  //var current = el.item.index;

	  	  //if you disable loop you have to comment this block
	  	  var count = el.item.count-1;
	  	  var current = Math.round(el.item.index - (el.item.count/2) - .5);

	  	  if(current < 0) {
	  	    current = count;
	  	  }
	  	  if(current > count) {
	  	    current = 0;
	  	  }

	  	  //end block

	  	  sync2
	  	    .find(".owl-item")
	  	    .removeClass("current")
	  	    .eq(current)
	  	    .addClass("current");
	  	  var onscreen = sync2.find('.owl-item.active').length - 1;
	  	  var start = sync2.find('.owl-item.active').first().index();
	  	  var end = sync2.find('.owl-item.active').last().index();

	  	  if (current > end) {
	  	    sync2.data('owl.carousel').to(current, 100, true);
	  	  }
	  	  if (current < start) {
	  	    sync2.data('owl.carousel').to(current - onscreen, 100, true);
	  	  }
	  	}

	  	function syncPosition2(el) {
	  	  if(syncedSecondary) {
	  	    var number = el.item.index;
	  	    sync1.data('owl.carousel').to(number, 100, true);
	  	  }
	  	}

	  	sync2.on("click", ".owl-item", function(e){
	  	  e.preventDefault();
	  	  var number = $(this).index();
	  	  sync1.data('owl.carousel').to(number, 300, true);
	  	});
	});

	// Star Rating
	$(document).ready(function(){

		var i;

	  	/* 1. Visualizing things on Hover - See next part for action on click */
	  	$('.cs_stars .cs_star').on('mouseover', function(){
	  	  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

	  	  // Now highlight all the stars that's not after the current hovered star
	  	  $(this).parent().children('.cs_star').each(function(e){
	  	    if (e < onStar) {
	  	      $(this).addClass('hover');
	  	    }
	  	    else {
	  	      $(this).removeClass('hover');
	  	    }
	  	  });

	  	}).on('mouseout', function(){
	  	  $(this).parent().children('.cs_star').each(function(e){
	  	    $(this).removeClass('hover');
	  	  });
	  	});


	  	/* 2. Action to perform on click */
	  	$('.cs_stars .cs_star').on('click', function(){
	  	  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	  	  var stars = $(this).parent().children('.cs_star');

	  	  for (i = 0; i < stars.length; i++) {
	  	    $(stars[i]).removeClass('selected');
	  	  }

	  	  for (i = 0; i < onStar; i++) {
	  	    $(stars[i]).addClass('selected');
	  	  }

	  	  var ratingValue = parseInt($(this).closest('.cs_star_select').find('span.selected').last().data('value'), 10);

	  	  // Put on text field
	  	  $(this).closest('.cs_star_select').find('input.csfield_input').val(ratingValue);


	  	});


	});



})(jQuery)

