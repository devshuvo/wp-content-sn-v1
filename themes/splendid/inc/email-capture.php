<?php

function get_new_referrer_email_html( $heading = false, $mailer ) {

	$template = 'emails/new-referral.php';

	return wc_get_template_html( $template, array(
		'email_heading' => $heading,
		'sent_to_admin' => false,
		'plain_text'    => false,
		'email'         => $mailer
	) );

}


/**
 * Email Capture
 */
function splendid_email_capture_shortcode( $atts ){

	extract(
		shortcode_atts( array(
			'type' => null,
			'send' => "Send",
			'ph' => "email",
			'action' => "",

		), $atts )
	);

	$classes = ($type == 2) ? " claim_my_offer_box " : " email_capture_box ";

	$btnclass = ($type == 2) ? " snec_submit button noradius " : " btn_link ";

	ob_start();	?>
	<div class="splendid_email_capture_wrap clear">
		<form method="post" id="splendid_email_capture" class="splendid_email_capture <?php echo $classes; ?>" action="<?php echo home_url('/offer-details/');?>">
			<div class="notice"></div>

			<?php if ( $action ) : ?>
				<input type="hidden" name="site_action" value="<?php esc_attr_e( $action ); ?>">
			<?php else: ?>
				<input type="hidden" name="site_action" value="default">
			<?php endif ?>

			<input type="email" name="EMAIL" id="email" placeholder="<?php echo esc_html( $ph ); ?>" required>
			<button type="submit" class=" <?php echo $btnclass; ?> "><?php echo esc_html( $send ); ?></button>
			<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

			<?php if( isset( $_GET['ref'] ) || $action == 'email_capture_process' ) : ?>
				<input type="hidden" name="redirect_to" value="<?php echo home_url('/offer-details/');?>">
			<?php else: ?>
				<input type="hidden" name="redirect_to" value="<?php echo home_url('/offer-details-2/');?>">
			<?php endif; ?>

			<?php if( isset( $_GET['ref'] ) ) : ?>
				<input type="hidden" name="by_ref" value="yes">
			<?php else: ?>
				<input type="hidden" name="by_ref" value="no">
			<?php endif; ?>

		</form>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'splendid_email_capture', 'splendid_email_capture_shortcode' );

/**
 * Handles AJAX for user register - referrer
 *
 * @return string
 */
add_action('wp_ajax_splendid_email_capture', 'splendid_email_capture_ajax_action');
add_action('wp_ajax_nopriv_splendid_email_capture', 'splendid_email_capture_ajax_action');
function splendid_email_capture_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$ref = get_option( 'yith_wcaf_referral_cookie_name', true );
	$user_email = sanitize_email( $_POST['EMAIL'] );
	$by_ref = sanitize_text_field( $_POST['by_ref'] );
	$action = sanitize_text_field( $_POST['site_action'] );

	if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = esc_url( $_POST['redirect_to'] );
    }

    if ( false == sn_email_exists( $user_email, 'ref' ) ) {
    	$response['status'] = 'success';
        $response['notice'] = __( 'Redirecting...', 'textdomain' );

	    $email_list = get_option('sn_email_list', true);
	    $email_list = $email_list && is_array($email_list) ? $email_list : array();

		$email_list[] = array(
	        'email' => $user_email,
	        'referrer_id' => isset( $_COOKIE[$ref] ) ? $_COOKIE[$ref] : "",
	        'by_ref' => $by_ref,
	        'time' => current_time('timestamp'),
	    );

		update_option( 'sn_email_list', $email_list );

		// Set email in cookie
		sn_setcookie( 'referrer_email', $_POST['EMAIL'] );

		if ( $action == 'boxdeal' && $by_ref != 'yes' ) {
			sn_setcookie( 'is_user_done_survey', sn_encrypt('0') );
	    	sn_setcookie( 'is_user_done_survey_claimed', sn_encrypt('0') );
		}

		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();

    		if ( $by_ref == 'yes' || $action == 'email_capture_process' ) {
    			update_user_meta( $user_id, 'new_referrer', '1' );
    			update_user_meta( $user_id, 'new_referrer_claimed', '0' );
    		}

    		if ( $action == 'boxdeal' && $by_ref != 'yes' ) {
				update_user_meta( $user_id, 'is_user_done_survey', '0' );
    			update_user_meta( $user_id, 'is_user_done_survey_claimed', '0' );
			}


    	} else {

    		if ( $by_ref == 'yes' || $action == 'email_capture_process' ) {
    			sn_setcookie( 'new_referrer', sn_encrypt('1') );
    			sn_setcookie( 'new_referrer_claimed', sn_encrypt('0') );
    		}

    	}

    } else {
        $response['status'] = 'error';
        $response['notice'] = __( 'Invalid user email', 'textdomain' );
    }

    $response['email_list'] = get_option('sn_email_list', true);

	// Json Response
	echo wp_json_encode( $response );

	die();
}