<?php

function get_customer_survey_email_html( $heading = false, $mailer, $formdata = null ) {

	$template = 'emails/customer-survey.php';

	return wc_get_template_html( $template, array(
		'email_heading' => $heading,
		'sent_to_admin' => false,
		'plain_text'    => false,
		'email'         => $mailer,
		'formdata'         => $formdata,
	) );

}


/**
 * Handles AJAX for user customer suevey
 *
 * @return string
 */
add_action('wp_ajax_customer_survey_form', 'splendid_customer_survey_form_ajax_action');
add_action('wp_ajax_nopriv_customer_survey_form', 'splendid_customer_survey_form_ajax_action');
function splendid_customer_survey_form_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$ref = get_option( 'yith_wcaf_referral_cookie_name', true );
	$referrer_email_c = isset( $_COOKIE['referrer_email'] ) ? $_COOKIE['referrer_email'] : "";
	$referrer_email_p = isset( $_POST['referrer_email'] ) ? $_POST['referrer_email'] : "";

    if ( $referrer_email_p ) {
    	$response['status'] = 'success';
        $response['notice'] = __( 'Redirecting...', 'textdomain' );

	    $customer_survey = get_option('sn_customer_survey_list', true);
	    $customer_survey = $customer_survey && is_array($customer_survey) ? $customer_survey : array();

		$customer_survey[] = array(
	        'email' => $referrer_email_p,
	        'product_rating' => isset( $_POST['product_rating'] ) ? $_POST['product_rating'] : "",
	        'level_of_focus' => isset( $_POST['level_of_focus'] ) ? $_POST['level_of_focus'] : "",
	        'level_of_focus_comment' => isset( $_POST['level_of_focus_comment'] ) ? $_POST['level_of_focus_comment'] : "",
	        'taste' => isset( $_POST['taste'] ) ? $_POST['taste'] : "",
	        'taste_comment' => isset( $_POST['taste_comment'] ) ? $_POST['taste_comment'] : "",
	        'recommend_friend' => isset( $_POST['recommend_friend'] ) ? $_POST['recommend_friend'] : "",
	        'review' => isset( $_POST['review'] ) ? $_POST['review'] : "",
	        'time' => current_time('timestamp'),
	    );

		update_option( 'sn_customer_survey_list', $customer_survey );

		sn_setcookie( 'is_user_done_survey', sn_encrypt('1') );


		/**
		 * Send HTML emails from WooCommerce.
		 *
		 */

		$mail_to = 'aktarujjman@gmail.com';

		// load the mailer class
		$mailer = WC()->mailer();
		$subject = "Splendid Nutrition: Survey Form";
		$message = "Thank you";
		$content = get_customer_survey_email_html( $subject, $mailer, $_POST );
		$headers = "Content-Type: text/html\r\n";
		//send the email through wordpress
		$mailer->send( $mail_to, $subject, $content, $headers );


    } else {
        $response['status'] = 'error';
        $response['notice'] = __( 'Something went wrong. Try again?', 'textdomain' );
    }

    if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = esc_url( $_POST['redirect_to'] );
    }

    $response['email_list'] = get_option('sn_customer_survey_list', true);


	// Json Response
	echo wp_json_encode( $response );

	die();
}


/**
 * Customer Survey HTML
 */
function customer_survey_form_shortcode( $atts ){

	ob_start();	?>

	<form id="customer_survey_form">

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("1"); ?>
			</div>
			<div class="cs_field_area">
				<p>Did you like Splendid Focus overall? How would you rate the product?</p>
				<?php cs_star_select('product_rating'); ?>
			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("2"); ?>
			</div>
			<div class="cs_field_area">
				<p>How did the product work for you? Did it it have an effect on your level of focus, mood, or mental energy?</p>
				<div class="sn_has_optional">
					<div class="sn_optional_rating">
						<?php cs_star_select('level_of_focus'); ?>
					</div>
					<div class="sn_optional_field">
						<textarea name="level_of_focus_comment" class="sn_textarea" placeholder="Add a comment (optional)"></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("3"); ?>
			</div>
			<div class="cs_field_area">
				<p>How did it taste?</p>
				<div class="sn_has_optional">
					<div class="sn_optional_rating">
						<?php cs_star_select('taste'); ?>
					</div>
					<div class="sn_optional_field">
						<textarea name="taste_comment" class="sn_textarea" placeholder="Add a comment (optional)"></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("4"); ?>
			</div>
			<div class="cs_field_area">
				<p>Would you recommend Splendid Focus to a friend?</p>

				<div class="cs_radio_select">
					<label>
						<input type="radio" name="recommend_friend" value="Yes">
						<div class="cs_select_text">Yes</div>
					</label>
					<label>
						<input type="radio" name="recommend_friend" value="No">
						<div class="cs_select_text">No</div>
					</label>
				</div>

			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("5"); ?>
			</div>
			<div class="cs_field_area">
				<p>Please leave a brief review of 100 characters or more:</p>
				<textarea name="review" class="sn_textarea sn_textarea_review" placeholder="Review"></textarea>
			</div>
		</div>

		<div class="notice"></div>

		<div class="cs_form_row clear">
			<div class="cs_field_area w100 text-center">
				<button type="submit" class="button cs_submit noradius">Complete!</button>
			</div>
		</div>

		<input type="hidden" name="redirect_to" value="<?php echo home_url( '/offer-details-2/' ); ?>">
		<?php if( isset( $_COOKIE['referrer_email'] ) ) : ?>
			<input type="hidden" name="referrer_email" value="<?php echo sn_get_cookie('referrer_email'); ?>">
		<?php endif; ?>

		<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

	</form>

	<?php
	return ob_get_clean();
}
add_shortcode( 'customer_survey_form', 'customer_survey_form_shortcode' );