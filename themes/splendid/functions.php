<?php

define('SPLENDID_IMG_DIR', get_stylesheet_directory_uri().'/assets/img');

add_action( 'wp_enqueue_scripts', 'splendid_enqueue_styles' );
function splendid_enqueue_styles() {
 	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'splendid_enqueue_child_styles' );
function splendid_enqueue_child_styles() {
	$ver = current_time( 'timestamp' );
 	wp_enqueue_style( 'owl-carousel', get_stylesheet_directory_uri() . '/assets/owl/owl.carousel.min.css' );

 	wp_dequeue_style( 'woocommerce-general' );
 	wp_deregister_style( 'woocommerce-general' );

 	wp_enqueue_style( 'woocommerce-general', get_stylesheet_directory_uri() . '/assets/css/woocommerce.css', array('woocommerce-smallscreen'), $ver );

 	wp_enqueue_style( 'splendid-theme', get_stylesheet_directory_uri() . '/assets/css/main.min.css', null, $ver );

 	wp_enqueue_script( 'owl-carousel', get_stylesheet_directory_uri() . '/assets/owl/owl.carousel.min.js', array('jquery') );
 	//wp_enqueue_script( 'jquery-stellar', get_stylesheet_directory_uri() . '/assets/js/jquery.stellar.min.js', array('jquery'), $ver, true );

 	wp_enqueue_script( 'splendid-theme', get_stylesheet_directory_uri() . '/assets/js/splendid-theme.js', array('jquery','owl-carousel'), $ver, true );

	wp_localize_script( 'splendid-theme', 'sn_ajax',
		array(
	        'nonce' => wp_create_nonce( 'ajax-nonce' ),
	        'url' => admin_url( 'admin-ajax.php' ),
	        'img_dir' => SPLENDID_IMG_DIR,
	        'get_rewards' => sn_get_rewards(),
	    )
	);

	wp_localize_script( 'splendid-theme', 'sn_ajax_params',
		array(
	        'img_dir' => SPLENDID_IMG_DIR,
	    )
	);
}

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function splendid_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( sn_decrypt(sn_get_cookie('is_user_done_survey')) == '1' ) {
		$classes[] = 'survey_completed';
	}

	return $classes;
}
add_filter( 'body_class', 'splendid_body_classes', 999 );

/**
 *	Custom Internal CSS function
 */
function splendid_internal_css(){
	$output = '';

	// Survey hide show
	if ( !isset($_GET['elementor-preview']) ) {


		if( sn_decrypt(sn_get_cookie('is_user_done_survey')) == '1' ) :
			$output .= '
				.hide_survey_completed {
					display:none;
				}
			';
		else:
			$output .= '
				.show_survey_completed {
					display:none;
				}
			';
		endif;

		$output .= '
			.survey-error-msg{
				display:none;
			}
		';
	}

	//$output .= '
	//	#elementor-preview .show_survey_completed,
	//	#elementor-preview .hide_survey_completed {
	//		display:block;
	//	}
	//';


	wp_add_inline_style( 'splendid-theme', $output );
}

add_action( 'wp_enqueue_scripts', 'splendid_internal_css'  );

/**
 * Required Files
 */
require_once dirname(__FILE__) . '/inc/svg-icons.php';
require_once dirname(__FILE__) . '/inc/email-capture.php';
require_once dirname(__FILE__) . '/inc/customer-survey.php';

/**
 * Remove password strength check.
 */
function splendid_wc_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'splendid_wc_remove_password_strength', 10 );

/**
 * Custom print_r function
 */
function ppr( $whattoprint ){
	echo "<pre>";
	print_r( $whattoprint );
	echo "</pre>";
}

/**
 * Footer Sticky Modal
 */
//add_action( 'wp_footer', 'splendid_body_hook' );
function splendid_body_hook(){ ?>
	<img src="<?php echo SPLENDID_IMG_DIR; ?>/hero_shape_3.svg" class="footer_modal_5" data-modal_id="dollar_five_modal">
	<?php
}

/**
 * Custom Widgets
 */
add_action( 'widgets_init', 'splendid_sidebar_registration');
function splendid_sidebar_registration() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<div class="screen-reader-text d-none">',
		'after_title'   => '</div>',
		'before_widget' => '',
		'after_widget'  => '',
	);

	// Sidebar #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Custom Header Left', 'astra' ),
				'id'          => 'splendid-header-widget-left',
				'description' => __( 'Widgets in this area will be displayed in header top left.', 'astra' ),
			)
		)
	);
	// Sidebar #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Custom Header Right', 'astra' ),
				'id'          => 'splendid-header-widget-right',
				'description' => __( 'Widgets in this area will be displayed in header top right.', 'astra' ),
			)
		)
	);

}


/**
 * Site Title / Logo
 *
 * @since 1.0.0
 */
function astra_site_branding_markup() {
	?>

	<div class="site-branding splendid-branding">
		<div
		<?php
			echo astra_attr(
				'site-identity',
				array(
					'class' => 'ast-site-identity',
				)
			);
		?>
		>
			<?php astra_logo(); ?>
			<div class="splendid-header-widget-left">
				<?php dynamic_sidebar( 'splendid-header-widget-left' ); ?>
			</div>
		</div>
		<div class="splendid-header-widget-right">
			<?php dynamic_sidebar( 'splendid-header-widget-right' ); ?>
		</div>
	</div>

	<!-- .site-branding -->
	<?php
}


add_action( 'wp_head', 'splendid_inline_css' );
function splendid_inline_css(){
	?>
	<style>
		.heart-svg:before{
			content: "";
			background-image: url(<?php echo SPLENDID_IMG_DIR; ?>/heart.svg);
		}
	</style>
	<?php
}



function splendid_after_theme_setup() {

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'primary-2' => esc_html__( 'Primary 2', 'splendid' ),
			'checkout' => esc_html__( 'Checkout Footer', 'splendid' ),
		)
	);

}

add_action( 'after_setup_theme', 'splendid_after_theme_setup' );

function sn_mobile_menu(){
	return wp_nav_menu(
		array(
			'theme_location' => 'primary-2',
			'menu_id'        => '',
			'menu_class'     => '',
			'container'      => '',

			'before'         => '',
			'after'          => '',
			'items_wrap'          => '<div class="hidden-mobile-menu" data-id="%1$s" data-class="%2$s">%3$s</div>',
			'echo'          => 0,
		)
	);
}

/**
 * Function to get Primary navigation menu
 */
function astra_primary_navigation_markup() {

	$disable_primary_navigation = astra_get_option( 'disable-primary-nav' );
	$custom_header_section      = astra_get_option( 'header-main-rt-section' );

	if ( $disable_primary_navigation ) {

		$display_outside = astra_get_option( 'header-display-outside-menu' );

		if ( 'none' != $custom_header_section && ! $display_outside ) {

			echo '<div class="main-header-bar-navigation ast-header-custom-item ast-flex ast-justify-content-flex-end">';
			/**
			 * Fires before the Primary Header Menu navigation.
			 * Disable Primary Menu is checked
			 * Last Item in Menu is not 'none'.
			 * Take Last Item in Menu outside is unchecked.
			 *
			 * @since 1.4.0
			 */
			do_action( 'astra_main_header_custom_menu_item_before' );

			echo astra_masthead_get_menu_items(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

			/**
			 * Fires after the Primary Header Menu navigation.
			 * Disable Primary Menu is checked
			 * Last Item in Menu is not 'none'.
			 * Take Last Item in Menu outside is unchecked.
			 *
			 * @since 1.4.0
			 */
			do_action( 'astra_main_header_custom_menu_item_after' );

			echo '</div>';

		}
	} else {

		$submenu_class = apply_filters( 'primary_submenu_border_class', ' submenu-with-border' );

		// Menu Animation.
		$menu_animation = astra_get_option( 'header-main-submenu-container-animation' );
		if ( ! empty( $menu_animation ) ) {
			$submenu_class .= ' astra-menu-animation-' . esc_attr( $menu_animation ) . ' ';
		}

		/**
		 * Filter the classes(array) for Primary Menu (<ul>).
		 *
		 * @since  1.5.0
		 * @var Array
		 */
		$primary_menu_classes = apply_filters( 'astra_primary_menu_classes', array( 'main-header-menu', 'ast-nav-menu', 'ast-flex', 'ast-justify-content-flex-end', $submenu_class ) );

		// Fallback Menu if primary menu not set.
		$fallback_menu_args = array(
			'theme_location' => 'primary',
			'menu_id'        => 'primary-menu',
			'menu_class'     => 'main-navigation',
			'container'      => 'div',

			'before'         => '<ul class="' . esc_attr( implode( ' ', $primary_menu_classes ) ) . '">',
			'after'          => '</ul>',
			'walker'         => new Astra_Walker_Page(),
		);

		$items_wrap  = '<nav ';
		$items_wrap .= astra_attr(
			'site-navigation',
			array(
				'id'         => 'site-navigation',
				'class'      => 'ast-flex-grow-1 navigation-accessibility',
				'aria-label' => esc_attr__( 'Site Navigation', 'astra' ),
			)
		);
		$items_wrap .= '>';
		$items_wrap .= '<div class="main-navigation">';
		$items_wrap .= '<ul id="%1$s" class="%2$s">'.sn_mobile_menu().' %3$s</ul>';
		$items_wrap .= '</div>'.do_shortcode( "[hfe_template id='365']");
		$items_wrap .= '</nav>';

		// Primary Menu.
		$primary_menu_args = array(
			'theme_location'  => 'primary',
			'menu_id'         => 'primary-menu',
			'menu_class'      => esc_attr( implode( ' ', $primary_menu_classes ) ),
			'container'       => 'div',
			'container_class' => 'main-header-bar-navigation',
			'items_wrap'      => $items_wrap,
		);

		if ( has_nav_menu( 'primary' ) ) {
			// To add default alignment for navigation which can be added through any third party plugin.
			// Do not add any CSS from theme except header alignment.
			echo '<div ' . astra_attr( 'ast-main-header-bar-alignment' ) . '>';
				wp_nav_menu( $primary_menu_args );
			echo '</div>';
		} else {

			echo '<div ' . astra_attr( 'ast-main-header-bar-alignment' ) . '>';
				echo '<div class="main-header-bar-navigation">';
					echo '<nav ';
					echo astra_attr(
						'site-navigation',
						array(
							'id' => 'site-navigation',
						)
					);
					echo ' class="ast-flex-grow-1 navigation-accessibility" aria-label="' . esc_attr__( 'Site Navigation', 'astra' ) . '">';
						wp_page_menu( $fallback_menu_args );
					echo '</nav>';
				echo '</div>';
			echo '</div>';
		}
	}

}


// hide coupon field on cart page
function hide_coupon_field_on_cart( $enabled ) {

	if ( is_cart() ) {
		$enabled = false;
	}

	return $enabled;
}
add_filter( 'woocommerce_coupons_enabled', 'hide_coupon_field_on_cart' );

// Remove Breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

function sn_single_product_archive_thumbnail_size( $size ){

	$size = 'full';

	return $size;


}
add_filter( 'single_product_archive_thumbnail_size', 'sn_single_product_archive_thumbnail_size', 10, 1 );


/**
 *  Product Popup Ajax Handle
 */
add_action('wp_ajax_nopriv_sn_product_detail_popup', 'sn_product_detail_popup_ajax_handle');
add_action('wp_ajax_sn_product_detail_popup', 'sn_product_detail_popup_ajax_handle');
function sn_product_detail_popup_ajax_handle() {
    // Check for nonce security
    $nonce = $_POST['nonce'];

    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) ){
        die ( 'Busted!');
    }

    $product_id = intval( $_POST['product_id'] );


    sn_body_popup_container_details( $product_id );

    die();
}

function sn_body_popup_container_details( $product_id = null ){

		if( !$product_id ){
			return;
		}

		add_action( 'snpopup_product_data', 'woocommerce_template_single_add_to_cart');

	    $wiqv_loop = new WP_Query(
	        array(
	            'post_type' => 'product',
	            'p' => $product_id,
	        )
	    );

		if( $wiqv_loop->have_posts() ) : ?>
			<div class="sn-product-wrap-outer">
				<div class="ast-container">
					<div class="sn-product-wrap">
						<?php while ( $wiqv_loop->have_posts() ) : $wiqv_loop->the_post(); ?>
							<?php do_action( 'snpopup_product_data' );
					 	endwhile;
					 	wp_reset_postdata(); ?>
					 </div>
				 </div>
			 </div>
		<?php endif; ?>
	<?php
}

/**
 * Close Button
 */
function sn_popup_close(){
	echo "<div class='close_btn_div'><a class='trigger_close_btn' href='#'>".splendid_svg('popup-cross')."</a></div>";
}

function sn_body_popup_container(){
	echo '<div class="sn_popup_container ast-container-fluid woocommerce">';

	echo '<div class="sn_popup_close_buttons">';
	echo "<div class='back_btn_div'><a class='trigger_back_btn' href='javascript:void(0)'>".splendid_svg('angle-left')."</a></div>";
	sn_popup_close();
	echo '</div>';

	splendid_logo_loader();

	sn_body_popup_container_details();
	echo '</div>';
}
add_action( 'wp_footer', 'sn_body_popup_container', 100 );


function sn_popup_product_data_before_atc(){

	if( is_single() && get_post_type()=='product' ){
		return;
	}

	global $product;
	$for_what_text = get_field('for_what_text') ? get_field('for_what_text') : "";
	$serving_info_text = get_field('serving_info_text') ? get_field('serving_info_text') : null;
	$free_shipping_tag_text = get_field('free_shipping_tag_text') ? get_field('free_shipping_tag_text') : null;

	?>
	<div class="snpopup-container">
		<div class="col-6">
			<div class="snpopup-content">
				<?php woocommerce_template_loop_product_title(); ?>
				<div class="after-title">
					<p><?php echo $for_what_text; ?></p>
					<p><?php echo $serving_info_text; ?></p>
					<p><?php echo $free_shipping_tag_text; ?></p>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="snpopup-thumbnail">
				<?php woocommerce_template_loop_product_thumbnail(); ?>
			</div>
		</div>
	</div>

	<?php
}
add_action( 'woocommerce_before_add_to_cart_button', 'sn_popup_product_data_before_atc', 10 );

function sn_popup_product_data_after_atcq(){

	if( is_single() && get_post_type()=='product' ){
		return;
	}

	global $product;
	$current_product_id = $product->get_id();

	$for_what_text = get_field('for_what_text') ? get_field('for_what_text') : "";
	$free_shipping_tag_text = get_field('free_shipping_tag_text') ? get_field('free_shipping_tag_text') : null;

	$serving_info_text = get_field('serving_info_text') ? get_field('serving_info_text') : null;
	$subscribe_save_value = get_field('subscribe_save_value') ? get_field('subscribe_save_value') : null;
	$serving_price_text = get_field('serving_price_text') ? get_field('serving_price_text') : null;
	$subscribesave_serving_info_text = get_field('subscribesave_serving_info_text') ? "(Subscribe & save $".$subscribe_save_value." <br>".get_field('subscribesave_serving_info_text').")" : null;

	$subscribesave_only_serving_info_text = get_field('subscribesave_serving_info_text') ? get_field('subscribesave_serving_info_text') : null;

	$subscribe_product_id = get_field('enable_subscription_syncing') && get_field('syncing_subscription_product') ? get_field('syncing_subscription_product') : $current_product_id;


	// get the "Checkout Page" URL
	$checkout_url = wc_get_checkout_url();
	$cart_url = wc_get_cart_url();

	$subs_product = wc_get_product( $subscribe_product_id );


	?>
	<span>Quantity</span>
	<div class="snpopup-price">
		<span class="price_subscribe show_on_otp show">
			<?php woocommerce_template_loop_price(); ?>
		</span>
		<span class="price_subscribe show_on_sns">
			<?php echo $subs_product->get_price_html(); ?>
		</span>

	</div>
	<div class="snpopup-container purchase_type-container text-center ov">
		<div class="col-6">
			<label class="radio-container">
			  	<input type="radio" checked="checked" name="purchase_type" value="0">
			  	<div class="checkmark">One-Time Purchase</div>

			</label>
			<div class="snpt-serving-text">(<?php echo $serving_price_text; ?>)</div>
		</div>
		<div class="col-6">
			<div class="pos_r">
				<label class="radio-container">
				  	<input type="radio" name="purchase_type" value="1">
				  	<div class="checkmark">Subscribe & Save $<?php echo $subscribe_save_value; ?></div>
				</label>
				<div class="tooltip"><?php echo splendid_svg('tooltip'); ?>
				  <span class="tooltiptext">Enjoy $5 off each item, and <br>FREE US shipping! <br>Ships once a month.<br>Cancel any time.</span>
				</div>
			</div>
			<div class="snpt-serving-text">(<?php echo $subscribesave_only_serving_info_text; ?>)</div>
		</div>
	</div>

	<div class="snpopup-container checkout_btn-container text-center">
		<div class="col-12">
			<div class="snpopup-fakespace"></div>
		</div>
	</div>

	<div class="snpopup-container checkout_btn-container text-center">
		<div class="col-6">
			<a href="<?php echo $checkout_url; ?>?add-to-cart=<?php echo $current_product_id; ?>" class="button alt direct_checkout_btn show_on_otp show">Checkout</a>
			<a href="<?php echo $checkout_url; ?>?add-to-cart=<?php echo $subscribe_product_id; ?>" class="button alt direct_checkout_btn show_on_sns">Checkout</a>
		</div>
		<div class="orbtn">Or</div>
		<div class="col-6">
			<a href="<?php echo $cart_url; ?>?add-to-cart=<?php echo $current_product_id; ?>" class="button alt direct_cart_btn show_on_otp show">Add to Cart</a>
			<a href="<?php echo $cart_url; ?>?add-to-cart=<?php echo $subscribe_product_id; ?>" class="button alt direct_cart_btn show_on_sns">Add to Cart</a>
		</div>
	</div>

	<?php
}
add_action( 'woocommerce_after_add_to_cart_quantity', 'sn_popup_product_data_after_atcq', 10 );
//add_action( 'woocommerce_after_add_to_cart_button', 'sn_popup_product_data', 10 );


/**
 * Logo Url
 *
 */
function splendid_logo_url(){
	if ( !get_theme_mod( 'custom_logo' ) ) {
		return;
	}

	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	return $image[0];
}

/**
 * Logo Loader
 */
function splendid_logo_loader(){
	?>
	<div class="splendid_logo_loader">
        <img src="<?php echo splendid_logo_url(); ?>">
    </div>
	<?php
}

function sn_after_theme_setup(){
	remove_theme_support( 'wc-product-gallery-slider' );
	remove_theme_support( 'wc-product-gallery-lightbox' );
	remove_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'sn_after_theme_setup', 99 );

/**
 * Handles AJAX for user register - referrer
 *
 * @return string
 */
add_action('wp_ajax_sn_register_form', 'sn_register_ajax_action');
add_action('wp_ajax_nopriv_sn_register_form', 'sn_register_ajax_action');
function sn_register_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$product_id = 133;
	$ref = get_option( 'yith_wcaf_referral_cookie_name', true );

	$user_email = sanitize_email( $_POST['email'] );
	$random_password = isset( $_POST['password'] ) ? $_POST['password'] : null;

	$parts = explode("@", $user_email);
	$user_name = sanitize_text_field( $parts[0] );

    $user_id = username_exists( $username );

    if ( ! $user_id && false == email_exists( $user_email ) ) {
        //$random_password = wp_generate_password( $length = 8, $include_standard_special_chars = false );

    	if ( $random_password ) {

		    // Create the WordPress User object with the basic required information
	        $user_id = wp_create_user( $user_name, $random_password, $user_email );

	        if (!$user_id || is_wp_error($user_id)) {
		       $response['status'] = 'error';
		       $response['error_log'] = $user_id->get_error_message();
		    } else {
		    	$response['status'] = 'success';

		    	// Set the global user object
				$current_user = get_user_by( 'id', $user_id );
				// set the WP login cookie
				$secure_cookie = is_ssl() ? true : false;
				wp_set_auth_cookie( $user_id, true, $secure_cookie );

				WC()->cart->add_to_cart( $product_id );

				if( sn_delete_cookie('new_referrer') ) {
					update_user_meta( $user_id, 'new_referrer', "1" );
					update_user_meta( $user_id, 'has_free_shipping', "1" );
					update_user_meta( $user_id, 'referred_by', sn_get_cookie($ref) );

					/**
					 * Send HTML emails from WooCommerce.
					 */
					// load the mailer class
					$mailer = WC()->mailer();

					$subject = "Splendid Nutrition: Referral Registration";
					$message = "Thank you for registration";
					$content = get_new_referrer_email_html( $subject, $mailer );
					$headers = "Content-Type: text/html\r\n";

					//send the email through wordpress
					$mailer->send( $user_email, $subject, $content, $headers );
				}

		    }
	    } else {
	    	$response['status'] = 'error';
        	$response['message'] = __( 'Password is required', 'splendid' );
	    }



    } else {
        $response['status'] = 'error';
        $response['error_log'] = __( 'User already exists. Please login', 'textdomain' );
    }

    if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = wc_get_account_endpoint_url('refer-friend');
    }


	// Json Response
	echo wp_json_encode( $response );

	die();
}


/**
 * Handles AJAX for Register Form - Checkout
 *
 * @return string
 */
add_action('wp_ajax_sn_register_form__checkout', 'sn_checkout_register_ajax_action');
add_action('wp_ajax_nopriv_sn_register_form__checkout', 'sn_checkout_register_ajax_action');
function sn_checkout_register_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$user_email = sanitize_email( $_POST['email'] );
	$random_password = isset( $_POST['password'] ) ? $_POST['password'] : null;
	$newsletter_subscribe = isset( $_POST['newsletter_subscribe'] ) ? true : false;
	$redirect_to = isset( $_POST['redirect_to'] ) ? esc_url( $_POST['redirect_to'] ) : null;

	$parts = explode("@", $user_email);
	$user_name = sanitize_text_field( $parts[0] );

    $user_id = username_exists( $username );

    if ( ! $user_id && false == email_exists( $user_email ) ) {
        //$random_password = wp_generate_password( $length = 8, $include_standard_special_chars = false );

        if ( $random_password ) {

	        // Create the WordPress User object with the basic required information
	        $user_id = wp_create_user( $user_name, $random_password, $user_email );

	        if (!$user_id || is_wp_error($user_id)) {
		       $response['status'] = 'error';
		       $response['message'] = $user_id->get_error_message();
		    } else {

		    	// Set the global user object
				$current_user = get_user_by( 'id', $user_id );
				// set the WP login cookie
				$secure_cookie = is_ssl() ? true : false;
				wp_set_auth_cookie( $user_id, true, $secure_cookie );

				update_user_meta( $user_id, 'newsletter_subscribe', $newsletter_subscribe );

		    	$response['status'] = 'success';
		    	$response['message'] = __( 'Account created. Redirecting...', 'splendid' );

			    if ( $redirect_to ) {
			    	$response['redirect_to'] = $redirect_to;
			    }

		    }
	    } else {
	    	$response['status'] = 'error';
        	$response['message'] = __( 'Password is required', 'splendid' );
	    }

    } else {
        $response['status'] = 'error';
        $response['message'] = __( 'User already exists. Please login', 'splendid' );
    }

	// Json Response
	echo wp_json_encode( $response );

	die();
}




/**
 * Handles AJAX for user login
 *
 */
function sn_ajax_login(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, is_ssl() );
    if ( !is_wp_error($user_signon) ){
        wp_set_current_user($user_signon->ID);
        wp_set_auth_cookie($user_signon->ID);
        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
    } else {
    	echo json_encode(array('loggedin'=>false, 'message'=>__('Incorrect password')));
    }

    die();
}
add_action( 'wp_ajax_ajaxlogin', 'sn_ajax_login' );
add_action( 'wp_ajax_nopriv_ajaxlogin', 'sn_ajax_login' );

/**
 * Give 5 Love area
 */
function splendid_give_5_love_area( $type = null ){ ?>

	<div class="giv5-love-area">
		<div class="giv5-love-img">
			<?php if( $type == "give5" ) : ?>
				<img src="<?php echo SPLENDID_IMG_DIR; ?>/bonus-tag.png" class="give5-bonus">
				<div class="give5-bonus hidden">
					<img src="<?php echo SPLENDID_IMG_DIR; ?>/bonus-tag.png?v" class="give5-bonus-img">
					<div class="give5-bonus-text">
						<h4>Bonus:</h4>
						<div class="fapb get-off">Get $<span class="uline">5 off</span> your next order<br> <span class="fapr">&</span> <span class="uline">FREE shipping</span></div>
						<div class="fapr when-you">when you sign up today!</div>
					</div>
				</div>
			<?php endif; ?>

			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-love.svg" class="give5-love">

			<div class="login-register-area">
				<div class="signup-htext">
					<h5>send the gift of focus <br>to a distracted friend</h5>
				</div>

				<div class="sn-form-area">

					<?php if( is_user_logged_in() && $type == "give5" ) : ?>
						<div id="proceed-signup">
	                        <form method="post" action="<?php echo wc_get_account_endpoint_url('refer-friend'); ?>">
	                            <div class="text-center">
	                            	<input type="hidden" name="become_an_affiliate" value="1">
	                                <button type="submit">Sign Up Now</button>
	                            </div>
	                        </form>
	                     </div>

					<?php else: ?>
	                     <div id="signup-form" data-toggle-hide>
	                     	<div class="sn-form-header">
	                     		<div class="form-type">
	                     			<p>Create account:</p>
	                     		</div>
	                     		<div class="frmh-text">
	                     			<p>Have an account? <span class="sn-form-toggle" data-toggle-show="#signin-form">Sign in</span></p>
	                     		</div>
	                     	</div>
	                        <form id="signup" method="post" autocomplete="off">
								<p class="status"></p>
	                            <input id="sn_signup-email" type="email" name="email" placeholder="user@account.com" value="<?php echo sn_get_referrer_email(); ?>" required>
	                            <input id="password" type="password" name="password" placeholder="Password" required>

	                            <div class="text-center">
	                                <button type="submit">Create</button>
	                            </div>

	                            <?php if ( (isset( $_POST['action'] ) && $_POST['action'] == "claim_my_offer_box") || sn_get_cookie('new_referrer') == 1 ) : ?>
	                            	<input type="hidden" name="refer_from" value="boxdeal_ref">
	                        	<?php endif; ?>

	                        	<input type="hidden" name="redirect_to" value="<?php echo wc_get_account_endpoint_url('refer-friend'); ?>?become_an_affiliate=1">

	                            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	                        </form>
	                     </div>

	                     <div id="signin-form" style="display:none" data-toggle-hide>
	                     	<div class="sn-form-header">
	                     		<div class="form-type">
	                     			<p>Sign in:</p>
	                     		</div>
	                     		<div class="frmh-text">
	                     			<p>Want to  <span class="sn-form-toggle" data-toggle-show="#signup-form">create a new account?</span></p>
	                     		</div>
	                     	</div>
	                         <form id="login" method="post" autocomplete="off">
	                             <p class="status"></p>
	                             <input id="username" type="text" name="username" placeholder="user@account.com">
	                             <input id="password" type="password" name="password" placeholder="Password">

	                             <div class="text-center">
	                                 <button class="submit_button" type="submit" value="Login" name="submit">Sign In</button>
	                             </div>

	                             <div class="text-center">
	                                 <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="forgot-pass"><?php esc_html_e( 'Forgot password?', 'splendid' ); ?></a>
	                             </div>

	                             <input type="hidden" name="redirect_to" value="<?php echo wc_get_account_endpoint_url('refer-friend'); ?>">

	                             <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	                         </form>
	                     </div>
	                 <?php endif; ?>

                 </div>

			</div>
		</div>
	</div>
	<!-- ./giv5-love-area -->
	<?php
}

/**
 * Offer details Bottom
 */
add_shortcode( 'offer_details_bottom', 'offer_details_bottom_shortcode' );
function offer_details_bottom_shortcode( $attr, $content = null ){
	extract(
		shortcode_atts( array(
			'type' => 'give5',
		), $atts )
	);

	ob_start(); ?>
	<div class="offer-details-wrap">
		<div class="offer-details-hfe">
			<div class="offer-details-hfe-inner">
				<?php echo do_shortcode("[hfe_template id='495']"); ?>
			</div>
		</div>
		<?php splendid_give_5_love_area('offer'); ?>
	</div>

	<?php
	return ob_get_clean();
}

/**
 * Homepage Popup
 */
add_action( 'wp_footer', 'homepage_give5_popup' );
function homepage_give5_popup(){
	if( is_front_page() || is_woocommerce() ) :	?>
	<div id="dollar_five_modal" class="sns-popup sns-popup-sm dark-bg">
		<?php sn_popup_close(); ?>
		<div class="sns-popup-inner">
			<?php echo do_shortcode( "[hfe_template id='620']" ); ?>
		</div>
	</div>
	<div class="close-popup-overlay" data-id="dollar_five_modal"></div>
	<?php endif; ?>

	<div id="buy_now_trigger" class="sns-popup sns-popup-md">
		<?php sn_popup_close(); ?>
		<div class="sns-popup-inner">
			<?php echo do_shortcode( "[hfe_template id='1124']" ); ?>
		</div>
	</div>
	<div class="close-popup-overlay" data-id="buy_now_trigger"></div>


	<?php
}

function dollar_five_modal_shortcode( $atts ){
	ob_start(); ?>
	<div class="dfms-wrap hide-on-phone">
		<div class="hero_shape_3" data-modal_id="dollar_five_modal">
			<?php echo splendid_svg('get_5_pulse'); ?>
		</div>
	</div>
	<?php return ob_get_clean();
}
add_shortcode( 'dollar_five_modal', 'dollar_five_modal_shortcode' );

function splendid_remove_wc_description_tab( $product_tabs ){
	unset( $product_tabs['description'] );

	return $product_tabs;
}
//add_filter( 'woocommerce_product_tabs', 'splendid_remove_wc_description_tab' );

/**
 * Order Received Page - info
 */
function order_ref_intro_shortcode( $atts ){
	ob_start();
	if( is_user_logged_in() && is_affiliate_enabled() ) : ?>
		Check out your referral dashboard for ways to easily <br>share $5 with friends and build rewards!
	<?php else: ?>
		Send them a free gift and get rewards (like $5, and a free box)!<br>We'll give you $5 and free shipping when you sign up today!
	<?php endif; ?>

	<?php
	return ob_get_clean();
}
add_shortcode( 'order_ref_intro', 'order_ref_intro_shortcode' );

/**
 * Order Received Page - Button Text
 */
function order_ref_btn_text_shortcode( $atts ){

	if( is_user_logged_in() && is_affiliate_enabled() ) {
		return esc_html__( 'Let\'s Go!', 'splendid' );
	} else{
		return esc_html__( 'Show Me How', 'splendid' );
	}

	return;

}
add_shortcode( 'order_ref_btn_text', 'order_ref_btn_text_shortcode' );

/**
 * Order Received Page - Button Link
 */
function order_ref_btn_link_shortcode( $atts ){

	if( is_user_logged_in() && is_affiliate_enabled() ) {
		return wc_get_account_endpoint_url('refer-friend');
	} else{
		return home_url('/friends/?redirect_from=checkout');
	}

	return;
}
add_shortcode( 'order_ref_btn_link', 'order_ref_btn_link_shortcode' );


function cs_star_select( $field = null ){
	?>
	<div class="cs_star_select">
		<input type="hidden" name="<?php echo esc_attr( $field ); ?>" class="csfield_input" id="csfield_<?php echo esc_attr( $field ); ?>">
		<div class="cs_stars">
			<?php for( $i = 1; $i <= 5; $i++ ) : ?>
				<span class="cs_star" data-value="<?php echo esc_attr( $i ); ?>">
					<svg width="47.934" height="45.662" version="1.1" viewBox="0 0 12.683 12.081" xmlns="http://www.w3.org/2000/svg"> <g transform="translate(-512.36 -1403.8)"> <path transform="matrix(.56054 0 0 .56 287.11 739.6)" d="m413.15 1186.5c0.63626 0 2.3773 7.0348 2.892 7.4088s7.7433-0.1441 7.9399 0.461c0.19661 0.6052-5.9559 4.4349-6.1526 5.04-0.19661 0.6051 2.5299 7.3198 2.0151 7.6938-0.51475 0.3739-6.0582-4.294-6.6945-4.294s-6.1798 4.6679-6.6945 4.294c-0.51475-0.374 2.2117-7.0887 2.0151-7.6938s-6.3492-4.4348-6.1526-5.04c0.19662-0.6051 7.4252-0.087 7.9399-0.461s2.2558-7.4088 2.892-7.4088z" fill="transparent" stroke="#1a1a1a" stroke-width=".94449"/> </g> </svg>
				</span>
			<?php endfor; ?>
		</div>
	</div>
	<?php
}


/**
 * Check email if exist
 */
function sn_email_exists( $email = null, $ref = null ){

	if ( !$email ) {
		return false;
	}

	// Get saved email list
	$sn_email_list = get_option('sn_email_list', true);
	$email_list = array();

	foreach ( $sn_email_list as $key => $val ) {
		$email_list[] = $val['email'];
	}

	if ( $ref == 'ref' ) {
		if ( in_array( $email, $email_list ) ) {
			return true;
		}
	} else {
		if ( email_exists( $email ) ) {
			return true;
		}
	}

	return false;
}


function woocommerce_pre_get_posts( $query ) {

  if ( ! is_admin() && is_post_type_archive( 'product' ) && $query->is_main_query() ) {

	$taxquery = array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_cat',
			'field'    => 'slug',
			'terms'    => 'shop_page',
		),
	);

	$query->set( 'tax_query', $taxquery );
  }
}
//add_action( 'pre_get_posts', 'woocommerce_pre_get_posts', 20 );

/**
 * WC allow weak pass
 */
add_filter( 'woocommerce_min_password_strength', 'reduce_min_strength_password_requirement' );
function reduce_min_strength_password_requirement( $strength ) {
    // 3 => Strong (default) | 2 => Medium | 1 => Weak | 0 => Very Weak (anything).
    return 0;
}

/**
 * WC Hook replacement
 */
// WOOCOMMERCE_CREATE_ACCOUNT_DEFAULT_CHECKED
//add_filter('woocommerce_create_account_default_checked', '__return_true');

// Hide
add_filter('woocommerce_shipping_not_enabled_on_cart_html', '__return_false');

// Order Review Form
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
add_action( 'woocommerce_after_checkout_form', 'woocommerce_order_review', 10 );

// Checkout Form
//remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
//add_action( 'woocommerce_review_order_after_cart_contents', 'woocommerce_checkout_coupon_form', 10 );

// Hook in actions once.
//add_action( 'woocommerce_checkout_billing', array( self::$instance, 'checkout_form_billing' ) );
//add_action( 'woocommerce_checkout_shipping', array( self::$instance, 'checkout_form_shipping' ) );
//remove_action( 'woocommerce_checkout_billing', array( new WC_Checkout, 'checkout_form_shipping' ), 10 );
//add_action( 'woocommerce_checkout_shipping', array( new WC_Checkout, 'checkout_form_shipping' ), 10 );

/**
 * Product Purchase type
 *
 * @usages: splendid_purchse_type( $_product->get_type() );
 */
function splendid_purchse_type( $get_type = null, $echo = true ){

	if ( !$get_type ) {
		return;
	}

	$output = "";

	if( $get_type == 'subscription' ){
		$output = esc_html__( 'Subscribe & Save', 'splendid' );
	} else {
		$output = esc_html__( 'One-Time Purchase', 'splendid' );
	}

	if ( $echo ) {
		echo $output;
	} else {
		return $output;
	}
}

/**
 * Splendid Checkout Content
 */
add_action( 'woocommerce_before_checkout_form', 'splendid_checkout_contents', 5);
function splendid_checkout_contents(){

	?>
	<div class="sn_checkout-title text-center">
		<div class="customer-info show_on_ci">
			<h2>Customer Information</h2>
		</div>
		<div class="payment-info show_on_pi">
			<h2>Payment Information</h2>
		</div>
	</div>
	<div class="sn_checkout-steps-wrap">
		<ul class="sn_checkout-steps">
			<li><a href="<?php echo esc_url( wc_get_cart_url() );?>"><span class="step-count">1</span> Cart</a></li>
			<li class="step-sep"><?php echo splendid_svg('step-sep'); ?></li>
			<li><a class="step-information" href="#"><span class="step-count">2</span> Information</a></li>
			<li class="step-sep"><?php echo splendid_svg('step-sep'); ?></li>
			<li><a class="step-payment inactive" href="#"><span class="step-count">3</span> Payment</a></li>
		</ul>
	</div>
	<div class="sn_notifications text-center">
		<!-- Notifications goes here -->
	</div>

	<!-- Express Checkout -->
	<div class='sn-express-checkout-wrap show_on_ci'><div class='sn-express-checkout'>
		<div class='sn-express-checkout-text fapr'>Express Checkout</div>
			<?php do_action( 'woocommerce_review_order_after_submit' ); ?>
		</div>

		<div class="sn_divider">
			<div class="sn_divider-text fapr gray">OR</div>
		</div>
	</div>
	<?php


}

/**
 * Checkout Bottom Menu
 *
 */
function splendid_checkout_menu(){
	?>
	<div class="sn-checkout-button process-to-payment-wrap text-center show_on_ci">
		<button type="button" class="button noradius process-to-payment fapb">Continue</button>
	</div>
	<div class="sn-checkout-button complete-order-wrap text-center show_on_pi">
		<button type="button" class="button noradius sn-complete-order fapb">Complete Order</button>

		<div class="subscrive-save-tos text-sm fapr text-left">
			By clicking "Complete Order", you confirm that your subscription will automatically renew
and your credit card will automatically be charged the subscription price according to the
order summary section of this page until you cancel your subscription. You can cancel
your subscription at any time.
		</div>
	</div>
	<?php if ( has_nav_menu( 'primary' ) ) :?>
		<div class="sn_checkout-menu">
		    <?php wp_nav_menu( array( 'theme_location' => 'checkout' ) ); ?>
		</div>
	<?php endif; ?>
	<?php
}
add_action( 'woocommerce_checkout_after_order_review', 'splendid_checkout_menu' );

/**
 * Checkout Customer Billing Info show
 *
 */
function splendid_customer_billing_info(){
	?>
	<div class="sn-customer-billing-info show_on_pi">

		<div class="sn-customer-billing-info-heading">
			<p class="text-lg">Customer & Shipping Information <span class="edit-billing-info">Edit</span></p>
		</div>

		<div class="gray fapr sncb-infos">

			<div class="sn-info sn-billing_email"><!--Email--></div>

			<div class="sn-info">
				<span class="sn-billing_first_name"><!--First Name--></span>
				<span class="sn-billing_last_name"><!--Last Name--></span>
			</div>

			<div class="sn-info">
				<span class="sn-billing_address_1"><!--Address--></span>
				<span class="sn-billing_address_2"><!--Apt, Suite--></span>
			</div>

			<div class="sn-info">
				<span class="sn-billing_city"><!--City--></span>
				<span class="sn-billing_state"><!--State--></span>
				<span class="sn-billing_postcode"><!--ZIP--></span>
			</div>

			<div class="sn-info sn-billing_country"><!--Country--></div>
		</div>

	</div>

	<?php
}
add_action( 'woocommerce_checkout_before_customer_details', 'splendid_customer_billing_info' );

function sn_cart_amazon_pay(){
	?>
	<button class="sn-amazon-pay">
		<?php echo splendid_svg('amazonpay'); ?>
	</button>
	<?php
}
add_action( 'woocommerce_proceed_to_checkout', 'sn_cart_amazon_pay', 20);
add_action( 'woocommerce_review_order_after_submit', 'sn_cart_amazon_pay', 5);


/**
 * Shop URL Shortcode
 */
function shop_url_shortcode( $atts ){
	return wc_get_page_permalink( 'shop' );
}
add_shortcode( 'shop_url', 'shop_url_shortcode' );

function remove_added_to_cart_notice()
{
    $notices = WC()->session->get('wc_notices', array());

    foreach( $notices['success'] as $key => &$notice){
        if( strpos( $notice, 'has been added' ) !== false){
            $added_to_cart_key = $key;
            break;
        }
    }
    unset( $notices['success'][$added_to_cart_key] );

    WC()->session->set('wc_notices', $notices);
}

/**
 * Raw URL After add to cart
 */
function sn_raw_url_redirect() {

	$ref = get_option( 'yith_wcaf_referral_var_name', true );

    if ( isset( $_GET['quantity'] ) || isset( $_GET['add-to-cart'] ) ) {
	    if ( is_cart() ) {
	    	wp_redirect( home_url( '/cart/') );
	    } elseif ( is_checkout() ) {
	    	wp_redirect( home_url( '/checkout/') );
	    }

	    remove_added_to_cart_notice();

        die;
    }

}
add_action( 'template_redirect', 'sn_raw_url_redirect' );


/**
 * Logged In Status on checkout page
 */
function checkout_logged_in_status(){

	if ( is_user_logged_in() || (isset( $_GET['loggedin'] ) && $_GET['loggedin'] == "true") ) {

		global $current_user;
		wp_get_current_user();

		$myaccount_url = get_permalink( wc_get_page_id( 'myaccount' ) );
		$username = "<a target='_blank' href='{$myaccount_url}'>{$current_user->display_name}</a>";

		$logout_url = esc_url( wc_logout_url() );
		$logout = "<a href='{$logout_url}'>Log out?</a>";

		$output = "<div class='sn_checkout_loggedin_text'>";
		$output .= "Logged in as <strong>{$username}</strong>. {$logout}";
		$output .= "</div>";

		return $output;
	}

	return;
}

function user_login_status_shortcode( $atts ){
	if ( is_user_logged_in() ) {

		global $current_user;
		wp_get_current_user();

		$myaccount_url = get_permalink( wc_get_page_id( 'myaccount' ) );
		$username = "<a target='_blank' href='{$myaccount_url}'>{$current_user->display_name}</a>";


		$output = "<div class='sn_user_login_status fapm'>";
		$output .= "<span>Hi</span> {$username}";
		$output .= "</div>";

		return $output;
	}
}
add_shortcode( 'user_login_status', 'user_login_status_shortcode' );

/**
 * Price filter
 */
add_filter( 'wc_price', 'sn_wc_price', 10, 5 );
function sn_wc_price( $return, $price, $args, $unformatted_price ) {
  $args = apply_filters(
	  'wc_price_args',
	  wp_parse_args(
	    $args,
	    array(
	      'ex_tax_label'       => false,
	      'currency'           => '',
	      'decimal_separator'  => wc_get_price_decimal_separator(),
	      'thousand_separator' => wc_get_price_thousand_separator(),
	      'decimals'           => wc_get_price_decimals(),
	      'price_format'       => get_woocommerce_price_format(),
	    )
	  )
	);

	$unformatted_price = $price;
	$negative          = $price < 0;
	$price             = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
	$price             = apply_filters( 'formatted_woocommerce_price', number_format( $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] ), $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] );

	if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $args['decimals'] > 0 ) {
	  $price = wc_trim_zeros( $price );
	}

	$formatted_price = ( $negative ? '-' : '' ) . sprintf( $args['price_format'], '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( $args['currency'] ) . '</span>', '<span class="sn_wc_price" data-price="' . $price . '">' . $price . '</span>' );
	$return          = '<span class="woocommerce-Price-amount amount"><bdi>' . $formatted_price . '</bdi></span>';

	if ( $args['ex_tax_label'] && wc_tax_enabled() ) {
	  $return .= ' <small class="woocommerce-Price-taxLabel tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
	}


	/**
	 * Filters the string of price markup.
	 *
	 * @param string $return            Price HTML markup.
	 * @param string $price             Formatted price.
	 * @param array  $args              Pass on the args.
	 * @param float  $unformatted_price Price as float to allow plugins custom formatting. Since 3.2.0.
	 */
	  return apply_filters( 'sn_wc_price', $return, $price, $args, $unformatted_price );
}

/**
 * Get a shipping methods full label including price.
 */
add_filter( 'woocommerce_cart_shipping_method_full_label', 'snwc_cart_totals_shipping_method_label', 10, 2 );
function snwc_cart_totals_shipping_method_label( $label, $method ) {
	$label     = '<span class="shipping-label" data-label="'.sanitize_title($method->get_label()).'">' . $method->get_label() . '</span>';
	$has_cost  = 0 < $method->cost;
	$hide_cost = ! $has_cost && in_array( $method->get_method_id(), array( 'free_shipping', 'local_pickup' ), true );

	if ( $has_cost && ! $hide_cost ) {
		if ( WC()->cart->display_prices_including_tax() ) {
			$label .= '<span class="shipping-label-sep">: </span>' . wc_price( $method->cost + $method->get_shipping_tax() );
			if ( $method->get_shipping_tax() > 0 && ! wc_prices_include_tax() ) {
				$label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
			}
		} else {
			$label .= '<span class="shipping-label-sep">: </span>' . wc_price( $method->cost );
			if ( $method->get_shipping_tax() > 0 && wc_prices_include_tax() ) {
				$label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
			}
		}
	}



	return $label;
}
