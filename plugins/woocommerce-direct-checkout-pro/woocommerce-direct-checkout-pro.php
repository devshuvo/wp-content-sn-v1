<?php
/**
 * Plugin Name: WooCommerce Direct Checkout PRO
 * Description: Simplifies the checkout process to improve your sales rate.
 * Version: 2.0.6
 * Author: QuadLayers
 * Author URI: https://www.quadlayers.com
 * Copyright:   2018 QuadLayers (https://www.quadlayers.com)
 * Text Domain: qlwcdc
 */
if (!defined('ABSPATH')) {
  die('-1');
}
if (!defined('QLWCDC_PRO_PLUGIN_NAME')) {
  define('QLWCDC_PRO_PLUGIN_NAME', 'WooCommerce Direct Checkout PRO');
}
if (!defined('QLWCDC_PRO_PLUGIN_VERSION')) {
  define('QLWCDC_PRO_PLUGIN_VERSION', '2.0.6');
}
if (!defined('QLWCDC_PRO_PLUGIN_FILE')) {
  define('QLWCDC_PRO_PLUGIN_FILE', __FILE__);
}
if (!defined('QLWCDC_PRO_PLUGIN_DIR')) {
  define('QLWCDC_PRO_PLUGIN_DIR', __DIR__ . DIRECTORY_SEPARATOR);
}
if (!defined('QLWCDC_PRO_DOMAIN')) {
  define('QLWCDC_PRO_DOMAIN', 'qlwcdc');
}
if (!defined('QLWCDC_PRO_WORDPRESS_URL')) {
  define('QLWCDC_PRO_WORDPRESS_URL', 'https://wordpress.org/plugins/woocommerce-direct-checkout/');
}
if (!defined('QLWCDC_PRO_REVIEW_URL')) {
  define('QLWCDC_PRO_REVIEW_URL', 'https://wordpress.org/support/plugin/woocommerce-direct-checkout/reviews/?filter=5#new-post');
}
if (!defined('QLWCDC_PRO_DEMO_URL')) {
  define('QLWCDC_PRO_DEMO_URL', 'https://quadlayers.com/portfolio/woocommerce-direct-checkout/?utm_source=qlwcdc_admin');
}
if (!defined('QLWCDC_PRO_PURCHASE_URL')) {
  define('QLWCDC_PRO_PURCHASE_URL', QLWCDC_PRO_DEMO_URL);
}
if (!defined('QLWCDC_PRO_SUPPORT_URL')) {
  define('QLWCDC_PRO_SUPPORT_URL', 'https://quadlayers.com/account/support/?utm_source=qlwcdc_admin');
}
if (!defined('QLWCDC_PRO_LICENSES_URL')) {
  define('QLWCDC_PRO_LICENSES_URL', 'https://quadlayers.com/account/licenses/?utm_source=qlwcdc_admin');
}
if (!defined('QLWCDC_PRO_GROUP_URL')) {
  define('QLWCDC_PRO_GROUP_URL', 'https://www.facebook.com/groups/quadlayers');
}

if (!class_exists('QLWCDC_PRO')) {

  class QLWCDC_PRO {

    protected static $instance;

    function add_action_links($links) {

      $links[] = '<a target="_blank" href="' . QLWCDC_PRO_SUPPORT_URL . '">' . esc_html__('Support', 'qlwcdc') . '</a>';
      $links[] = '<a target="_blank" href="' . QLWCDC_PRO_LICENSES_URL . '">' . esc_html__('License', 'qlwcdc') . '</a>';

      return $links;
    }

    function add_admin_notices() {

      $screen = get_current_screen();

      if (isset($screen->parent_file) && 'plugins.php' === $screen->parent_file && 'update' === $screen->id) {
        return;
      }

      $plugin = 'woocommerce-direct-checkout/woocommerce-direct-checkout.php';

      if (is_plugin_active($plugin)) {
        return;
      }

      if ($this->is_installed()) {

        if (!current_user_can('activate_plugins')) {
          return;
        }
        ?>
        <div class="error">
          <p>
            <a href="<?php echo wp_nonce_url('plugins.php?action=activate&amp;plugin=' . $plugin . '&amp;plugin_status=all&amp;paged=1', 'activate-plugin_' . $plugin); ?>" class='button button-secondary'><?php _e('Activate WooCommerce Direct Checkout', 'qlwcdc'); ?></a>
            <?php esc_html_e('WooCommerce Direct Checkout PRO not working because you need to activate the WooCommerce Direct Checkout plugin.', 'qlwcdc'); ?>   
          </p>
        </div>
        <?php
      } else {
        if (!current_user_can('install_plugins')) {
          return;
        }
        ?>
        <div class="error">
          <p>
            <a href="<?php echo wp_nonce_url(self_admin_url('update.php?action=install-plugin&plugin=woocommerce-direct-checkout'), 'install-plugin_woocommerce-direct-checkout'); ?>" class='button button-secondary'><?php _e('Install WooCommerce Direct Checkout', 'qlwcdc'); ?></a>
            <?php esc_html_e('WooCommerce Direct Checkout PRO not working because you need to install the WooCommerce Direct Checkout plugin.', 'qlwcdc'); ?>
          </p>
        </div>
        <?php
      }
    }

    function is_installed() {

      $installed_plugins = get_plugins();

      return isset($installed_plugins['woocommerce-direct-checkout/woocommerce-direct-checkout.php']);
    }

    /* function quick_purchase_add_to_cart_message_html($message, $products, $show_qty) {

      if (isset($_GET['add-to-cart'])) {
      if (is_cart()) {
      $message = str_replace(esc_url(wc_get_page_permalink('cart')), esc_url(wc_get_page_permalink('checkout')), $message);
      $message = str_replace(esc_html__('View cart', 'woocommerce'), esc_html__('Checkout', 'woocommerce'), $message);
      }
      if (is_checkout()) {
      $message = str_replace(esc_url(wc_get_page_permalink('cart')), esc_url(wc_get_page_permalink('checkout') . '/#order_review'), $message);
      $message = str_replace(esc_html__('View cart', 'woocommerce'), esc_html__('Place order', 'woocommerce'), $message);
      }
      }

      return $message;
      } */

    function add_updater() {

      global $qlwcdc_updater;

      if (include_once 'includes/updater.php') {

        $qlwcdc_updater = qlwdd_updater(array(
            'api_url' => 'https://quadlayers.com/wc-api/qlwdd/',
            'plugin_url' => QLWCDC_PRO_DEMO_URL,
            'plugin_file' => __FILE__,
            'license_key' => get_option('qlwcdc_license_key'),
            'license_email' => get_option('qlwcdc_license_email'),
            'license_url' => admin_url('admin.php?page=wc-settings&tab=' . QLWCDC_DOMAIN . '&section=license'),
            'product_key' => '16cb9ea2107b1ac236800dd5168c3c0f',
        ));
      }
    }

    function includes() {
      if (class_exists('QLWCDC')) {
        require_once('includes/archives.php');
        require_once('includes/products.php');
        require_once('includes/checkout.php');
        require_once('includes/license.php');
      }
    }

    function init() {

      if (class_exists('QLWCDC')) {
        add_action('admin_init', array($this, 'add_updater'));
        remove_action('admin_footer', array(QLWCDC::instance(), 'remove_premium'));
      }

      add_action('admin_notices', array($this, 'add_admin_notices'));
      add_filter('plugin_action_links_' . plugin_basename(QLWCDC_PRO_PLUGIN_FILE), array($this, 'add_action_links'));
    }

    public static function instance() {
      if (!isset(self::$instance)) {
        self::$instance = new self();
        self::$instance->includes();
        self::$instance->init();
      }
      return self::$instance;
    }

  }

  add_action('plugins_loaded', array('QLWCDC_PRO', 'instance'), 99);
}