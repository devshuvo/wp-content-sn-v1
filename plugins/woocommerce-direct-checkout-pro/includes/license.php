<?php
if (!defined('ABSPATH')) {
  die('-1');
}

if (!class_exists('QLWCDC_PRO_License')) {

  class QLWCDC_PRO_License {

    protected static $instance;

    function add_section($sections) {

      unset($sections['premium']);

      $sections['license'] = __('License', 'qlwcdc');

      return $sections;
    }

    function add_fields($settings) {

      global $current_section;

      if ('license' == $current_section) {

        $settings = array(
            'qlwcdc_section_title' => array(
                'name' => __('License', 'qlwcdc'),
                'type' => 'title',
                'desc' => __('Add your license key to activate the premium features.', 'qlwcdc'),
                'id' => 'qlwcdc_section_title'
            ),
            'add_license_key' => array(
                'name' => __('Key', 'qlwcdc'),
                'id' => 'qlwcdc_license_key',
                'type' => 'text',
                'placeholder' => esc_html__('Enter your license key', 'qlwcdc'),
                'default' => ''
            ),
            'add_license_email' => array(
                'name' => __('Email', 'qlwcdc'),
                'id' => 'qlwcdc_license_email',
                'type' => 'text',
                'placeholder' => esc_html__('Enter your license email', 'qlwcdc'),
                'default' => ''
            ),
            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'qlwcdc_products_section_end'
            )
        );
      }

      return $settings;
    }

    function add_license() {

      global $current_section, $qlwcdc_updater;

      if ('license' == $current_section) {
        ?>
        <table class="form-table" cellspacing="0">
          <tbody>
            <?php if ($activation = $qlwcdc_updater->get_activation()) : ?>
              <?php if (!empty($activation->success)) : ?>
                <tr valign="top">
                  <th scope="row" class="titledesc"><?php _e('Created', 'qlwdd') ?></th>
                  <td><?php echo date(get_option('date_format'), strtotime($activation->license_created)) ?></td>
                </tr>
                <tr valign="top">
                  <th scope="row" class="titledesc"><?php _e('Limit', 'qlwdd') ?></th>
                  <td><?php echo $activation->license_limit ? esc_attr($activation->license_limit) : esc_html__('Unlimited', 'qlwdd'); ?></td>
                </tr>
                <tr valign="top">
                  <th scope="row" class="titledesc"><?php _e('Activations', 'qlwdd') ?></th>
                  <td><?php echo esc_attr($activation->activation_count); ?></td>
                </tr>
                <tr valign="top">
                  <th scope="row" class="titledesc"><?php _e('Updates', 'qlwdd') ?></th>
                  <td><?php echo ($activation->license_expiration != '0000-00-00 00:00:00' && $activation->license_updates) ? sprintf(__('Expires on %s', 'qlwdd'), $activation->license_expiration) : esc_html__('Unlimited', 'qlwdd'); ?></td>
                </tr>
                <tr valign="top">
                  <th scope="row" class="titledesc"><?php _e('Support', 'qlwdd') ?></th>
                  <td><?php echo ($activation->license_expiration != '0000-00-00 00:00:00' && $activation->license_support) ? sprintf(__('Expires on %s', 'qlwdd'), $activation->license_expiration) : esc_html__('Unlimited', 'qlwdd'); ?></td>
                </tr>
                <tr valign="top">
                  <th scope="row" class="titledesc"><?php _e('Expiration', 'qlwdd') ?></th>
                  <td><?php echo ($activation->license_expiration != '0000-00-00 00:00:00') ? date_i18n(get_option('date_format'), strtotime($activation->license_expiration)) : __('Unlimited', 'qlwdd'); ?></td>
                </tr>
              <?php endif; ?>
              <tr valign="top">
                <th scope="row" class="titledesc"><?php esc_html_e('Status', 'qlwcdc'); ?></th>
                <td><?php echo esc_html($activation->message); ?></td>
              </tr>
            <?php endif; ?>
            <tr valign="top">
              <th scope="row" class="titledesc"><?php esc_html_e('Message', 'qlwcdc'); ?></th>
              <td scope="row" class="titledesc">
                <p class="description">
                  <?php if (empty($activation->activation_instance)): ?>
                    <?php printf(__('Before you can receive plugin updates, you must first authenticate your license. To locate your License Key, <a href="%s" target="_blank">log in</a> to your account and navigate to the <strong>Account > Licenses</strong> page.', 'qlwcdc'), QLWCDC_PRO_LICENSES_URL); ?>
                  <?php else: ?>
                    <?php printf(__('Thanks for register your license! If you have doubts you can request <a href="%s" target="_blank">support</a> through our ticket system.', 'qlwcdc'), QLWCDC_PRO_SUPPORT_URL); ?>
                  <?php endif; ?>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <?php
      }
    }

    function save_license_activation() {

      global $qlwcdc_updater;

      if (isset($_POST['save']) && !empty($_POST['qlwcdc_license_key']) && !empty($_POST['qlwcdc_license_email'])) {
        $qlwcdc_updater->request_activation($_POST['qlwcdc_license_key'], $_POST['qlwcdc_license_email']);
      }
    }

    function init() {
      add_filter('qlwcdc_add_sections', array($this, 'add_section'));
      add_filter('qlwcdc_add_fields', array($this, 'add_fields'));
      add_action('woocommerce_sections_qlwcdc', array($this, 'add_license'), 99);
      add_action('woocommerce_settings_save_qlwcdc', array($this, 'save_license_activation'));
    }

    public static function instance() {
      if (!isset(self::$instance)) {
        self::$instance = new self();
        self::$instance->init();
      }
      return self::$instance;
    }

  }

  QLWCDC_PRO_License::instance();
}