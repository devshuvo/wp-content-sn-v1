<?php
if (!defined('ABSPATH')) {
  die('-1');
}

if (!class_exists('QLWCDC_PRO_Products')) {

  class QLWCDC_PRO_Products extends QLWCDC_Products {

    protected static $instance;

    function save_product_options($product_id) {

      $this->add_product_fields();

      if ($product = wc_get_product($product_id)) {

        foreach ($this->product_fields as $field) {

          if (isset($field['id']) && isset($_POST[$field['id']])) {

            $value = esc_attr(trim(stripslashes($_POST[$field['id']])));

            if ($value != get_option($field['id'], true)) {
              $product->update_meta_data($field['id'], $value);
            } else {
              $product->delete_meta_data($field['id']);
            }
          }
        }

        $product->save();
      }
    }

    function get_quick_purchase_link($product_id = 0) {

      if ('checkout' === $this->get_product_option($product_id, 'qlwcdc_add_product_quick_purchase_to', 'checkout')) {
        return wc_get_checkout_url();
      }

      return wc_get_cart_url();
    }

    function add_to_cart_redirect($url) {

      if (isset($_GET['add-to-cart']) && absint($_GET['add-to-cart']) > 0 && strpos(home_url($_SERVER['REQUEST_URI']), $this->get_quick_purchase_link()) !== false) {
        return false;
      }

      return $url;
    }

    function add_quick_purchase_button() {

      global $product;

      static $instance = 0;

      if (!$instance && 'yes' === $this->get_product_option($product->get_id(), 'qlwcdc_add_product_quick_purchase', 'no')) {
        ?>
        <a class="single_add_to_cart_button qlwcdc_quick_purchase button <?php echo esc_attr($this->get_product_option($product->get_id(), 'qlwcdc_add_product_quick_purchase_class')); ?>" href="<?php echo $this->get_quick_purchase_link($product->get_id()); ?>"><?php esc_html_e($this->get_product_option($product->get_id(), 'qlwcdc_add_product_quick_purchase_text', esc_html__('Purchase Now', 'qlwcdc'))); ?></a>
        <?php
        $instance++;
      }
    }

    function add_product_default_attributes() {

      if ('yes' === get_option('qlwcdc_add_product_default_attributes')) {

        global $product;

        if (!count($default_attributes = get_post_meta($product->get_id(), '_default_attributes'))) {

          $new_defaults = array();

          $product_attributes = $product->get_attributes();

          if (count($product_attributes)) {

            foreach ($product_attributes as $key => $attributes) {

              $values = explode(',', $product->get_attribute($key));

              if (isset($values[0]) && !isset($default_attributes[$key])) {
                $new_defaults[$key] = sanitize_key($values[0]);
              }
            }

            update_post_meta($product->get_id(), '_default_attributes', $new_defaults);
          }
        }
      }
    }

    function init() {
      add_action('woocommerce_process_product_meta', array($this, 'save_product_options'));
      add_filter('woocommerce_add_to_cart_redirect', array($this, 'add_to_cart_redirect'), 10);
      add_action('woocommerce_after_add_to_cart_button', array($this, 'add_quick_purchase_button'), -5);
      add_action('woocommerce_before_single_product_summary', array($this, 'add_product_default_attributes'));
    }

    public static function instance() {
      if (!isset(self::$instance)) {
        self::$instance = new self();
        self::$instance->init();
      }
      return self::$instance;
    }

  }

  QLWCDC_PRO_Products::instance();
}