Author: QuadLayers
License: Copyright

== Changelog ==

= 2.0.6 =

* Fix: updater 2.0.6

= 2.0.5 =

* Fix: updater 2.0.5

= 2.0.4 =

* Fix: quick checkout purchase button action order

= 2.0.3 =

* Fix: quick checkout purchase button

= 2.0.2 =

* Fix: undefined index add-to-cart
* New. updater 2.0.2

= 2.0.1 =

* Fix: quick purchase button

= 2.0.0 =

* First release!