﻿=== Multilevel Referral Affiliate Plugin for WooCommerce ===
Plugin Name: Multilevel Referral Affiliate Plugin for WooCommerce
Plugin URI: http://referral.staging.prismitsystems.com
Contributors: prismitsystems
Donate link: http://www.prismitsystems.com/
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: woocommerce, referral, multilevel users, credit points, redeemption, redeem Points, Invite, invite friends, earning, earn credit points, commision, join referral program 
Requires at least: 4.0
Requires PHP: 5.6
Tested up to: 5.4.1
Stable tag: 1.1

The most advanced referral Plugin for woocommerce users who wants to increase their sale within few days.

== Description ==

**Attract new customers, grow and market your business for free using a social referral program. Made especially for WooCommerce store owners, WooCommerce Multilevel Referral Plugin rewards your clients for sharing your website with their friends, family, and colleagues.**

One of the most recommended referral marketing programs on WooCommerce, it’s really easy to configure and use. You can track the referrers, their sales, their total credits as well – along with redeemed credits to know your clientele well.

Moreover, you can even send emails to referral users reminding them of their referral points by that, appealing them into increasing their purchase. We thereby help you boost your sales by word-of-mouth as well as enticing the already registered to make them purchase more.

The referral plugin is compatible with the latest WordPress as well as WooCommerce versions.

= How does the Multilevel Referral Plugin work? =

* Your client will register on the website and join referral program receive a referral code.
* He/She will then share the code with his/her relatives and friends.
* Once his/her connection register on the website, join referral program using his/her code and purchases a product, he/she will be entitled to receive a referral reward.
* The more number of people are referred to in his/her hierarchy, the more his/her commission would be.

[youtube https://youtu.be/3DBfNhjxTDo]

== Features == 

* Increase sales through a referral chain.
* User can set Store credit percentage globally.
* Users can earn credit points whilst their followers buy products from the existing online store.
* Redeem your points at the time of personal purchase.
* Customised email templates.
* Set Auto Join or Manual join option.
* See your earned point in account area.
* Invite friends through social media.
* Referrers can share predefined banners on social media for increasing their network.
* Admin can see a complete list of registered users who have joined their referral program with their earned credit points.
* The log is maintained for earned credits as well as redeemed points for each order. Admin can see this log within the Admin panel.

== Premium Features ==

* **Level Based Credit System** - Store owners can manage levelwise credit percentage globally (Applicable for all products), Category Wise and even product specific. 
* **Binary MLM System** - Added support for Binary MLM system, so site owner can choose regular or Binary MLM system base on their requirement.
* **Manage number of levels** - Store owners can define the number of referrer levels which the referrers would receive the credit points for.
From the admin panel, store owners can manage levels and their credit percentage.
* Ability to set validation period for credit points. After the validation period the credit points will expire.
* Admin can set number of days to send notification emails before the point expire. This will enable the user to have enough time for him/her to take appropriate action before the credits expire.
* Products can be excluded from the referral program.
* Admin can set monthly credit limit, and also there is an ability to set monthly redemption limit. 
* The multi level user list view is provided so; admin can see hierarchy of users.

Interested in the premium features? You can read all about it [here](http://referral.staging.prismitsystems.com/docs/).

[Demo Plugin](http://referral.staging.prismitsystems.com/) 

Now credit point information and invite friend form can be show anywhere on wordpress pages. These shortcodes will show information of users who are logged in.

[wmc_invite_friends] =  this shortcode will show invite friend form.

[wmc_show_credit_info] = this will show logged in user credit point information.

== Screenshots ==
1. Listing of all referral users.
2. Referral plugin settings in woocommerce setting panel.
3. Point logs of all referral users.
4. You can set email content for various email templates.
5. Upload promotional banner from here.
6. User can see their credit points from user account. They can invite their friends by email address or sharing on social media.
7. Check this option to enable referral options on registration form.
8. Referral options on registration form.
9. Level Wise Credit System. (Premium Feature)
10. Multilevel Recursive Credit System. (Premium Feature)

== Installation ==
1. Upload the 'Multilevel Referral Affiliate Plugin for WooCommerce' plugin to the "/wp-content/plugins/" directory.
1. Activate the plugin through the "Plugins" menu in WordPress.

== Changelog ==
= 1.0 =
* Initial release.

= 1.1 =
* Fixed - Fixed type mistakes and wannings.
* New - Add Binay MLM support for pro version.