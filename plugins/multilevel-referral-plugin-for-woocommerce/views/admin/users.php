<h3><?php echo __('Referral Program Statistics', 'multilevel-referral-affiliate'); ?></h3>

<table class="form-table">

	<tr>

		<th><label for="join_date"><?php echo __('Join Date', 'multilevel-referral-affiliate')?></label></th>

		<td>

			<input type="text" name="join_date" id="join_date" disabled value="<?php echo isset( $user['join_date'] ) ? $user['join_date'] : ''; ?>" class="regular-text" /><br />

			<span class="description"><?php echo __('Joining date of referral user', 'multilevel-referral-affiliate');?>.</span>

		</td>

	</tr>

	<tr>

		<th><label for="referal_benefits"><?php echo __('Referral Discount', 'multilevel-referral-affiliate')?></label></th>

		<td>

			<input type="checkbox" name="referal_benefits" disabled id="referal_benefits" <?php echo isset( $user['referal_benefits'] ) ? esc_attr($user['referal_benefits'] ) ? 'checked' : '' : '' ; ?> /><br />

			<span class="description"><?php echo __('Status of referral user for discount that taken or not?', 'multilevel-referral-affiliate');?>.</span>

		</td>

	</tr>

	<tr>

		<th><label for="referal_code"><?php echo __('Referral code', 'multilevel-referral-affiliate')?></label></th>

		<td>

			<input type="text" name="referal_code" id="referal_code" disabled value="<?php echo isset( $user['referral_code'] ) ? esc_attr( $user['referral_code'] ) : ''; ?>" class="regular-text" /><br />

			<span class="description"><?php echo __('Auto generated referral code for referral users', 'multilevel-referral-affiliate');?>.</span>

		</td>

	</tr>
</table>