<div class="wrap">

	<h1><?php echo __('Multilevel Referral Affiliate Plugin for WooCommerce', 'multilevel-referral-affiliate')?></h1>

	<div id="referral_program_statistics">

		<div class="total_users_panel">

			<div class="icon">

				<span class="dashicons dashicons-groups"></span>	

			</div>

			<div class="number"><?php echo $data['total_users'];?></div>

			<div class="text"><?php echo __('Total Users','multilevel-referral-affiliate');?></div>

		</div>

		<div class="total_referral_panel">

			<div class="icon">

				<span class="dashicons dashicons-networking"></span>	

			</div>

			<div class="number"><?php echo $data['total_referrals'];?></div>

			<div class="text"><?php echo __('Referrals','multilevel-referral-affiliate');?></div>

		</div>

		<div class="total_earn_panel">

			<div class="icon">

				<span class="dashicons dashicons-download"></span>	

			</div>

			<div class="number"><?php echo $data['total_credites'];?></div>

			<div class="text"><?php echo __('Earned Credits','multilevel-referral-affiliate');?></div>

		</div>

		<div class="total_redeem_panel">

			<div class="icon">

				<span class="dashicons dashicons-upload"></span>	

			</div>

			<div class="number"><?php echo $data['total_redeems'];?></div>

			<div class="text"><?php echo __('Redeemed Credits','multilevel-referral-affiliate');?></div>

		</div>

	</div>

	

<h2 class="nav-tab-wrapper">	

    <a href="<?php echo admin_url('admin.php?page=wc_referral'); ?>" title="<?php echo __('Referral users','multilevel-referral-affiliate');?>" class="nav-tab <?php echo !isset($_GET['tab']) ? 'nav-tab-active' : ''; ?>"><?php echo __('Referral users','multilevel-referral-affiliate');?></a>

	<a href="<?php echo admin_url('admin.php?page=wc_referral&tab=orderwise_credits'); ?>" title="<?php echo __('Orderwise user credits','multilevel-referral-affiliate');?>" class="nav-tab <?php echo isset($_GET['tab']) && sanitize_text_field($_GET['tab']) == 'orderwise_credits' ? 'nav-tab-active' : ''; ?>"><?php echo __('Orderwise user credits','multilevel-referral-affiliate');?></a>

	<a href="<?php echo admin_url('admin.php?page=wc_referral&tab=credit_logs'); ?>" title="<?php echo __('Point logs','multilevel-referral-affiliate');?>" class="nav-tab <?php echo isset($_GET['tab']) && sanitize_text_field($_GET['tab']) == 'credit_logs' ? 'nav-tab-active' : ''; ?>"><?php echo __('Point logs','multilevel-referral-affiliate');?></a>

	<a href="<?php echo admin_url('admin.php?page=wc_referral&tab=email_templates'); ?>" title="<?php echo __('Email templates','multilevel-referral-affiliate');?>" class="nav-tab <?php echo isset($_GET['tab']) && sanitize_text_field($_GET['tab']) == 'email_templates' ? 'nav-tab-active' : ''; ?>"><?php echo __('Email templates','multilevel-referral-affiliate');?></a>

    <a href="<?php echo admin_url('edit.php?post_type=wmc-banner'); ?>" title="<?php echo __('Banners','multilevel-referral-affiliate');?>" class="nav-tab <?php echo isset($_GET['tab']) && sanitize_text_field($_GET['tab']) == 'banners' ? 'nav-tab-active' : ''; ?>"><?php echo __('Banners','multilevel-referral-affiliate');?></a>    

    <a href="#" title="<?php echo __('Advance Settings','multilevel-referral-affiliate');?>" class="premium_featured nav-tab <?php echo isset($_GET['tab']) && sanitize_text_field($_GET['tab']) == 'advSettings' ? 'nav-tab-active' : ''; ?>"><?php echo __('Advance Settings','multilevel-referral-affiliate');?></a>

    <?php if(!isset($_GET['tab'])  && !isset($_GET['user_status'])){ 

        echo '<div class="in_active_user_panel"><a class="button-secondary" href="'.admin_url('admin.php?page=wc_referral&user_status=0').'">'.__('Deleted Referrals','multilevel-referral-affiliate').'</a></div>';

    }?>   

</h2>