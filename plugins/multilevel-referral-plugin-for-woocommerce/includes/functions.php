<?php

function wmc_custom_sanitize_array( &$array ) {
    if( is_array($array) && count($array) > 0 ){
        foreach ($array as &$value) {   
            if( !is_array($value) ) {
                $value = sanitize_text_field( $value );
            } else {
                wmc_custom_sanitize_array($value);
            }
        }
    }
    return $array;
} 

?>