<?php
/**
 * WooCommerce Multilevel Referral General Settings
 *
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
 
if ( ! class_exists( 'WMR_Settings_General' )) :

/**
 * WC_Admin_Settings_General.
 */
class WMR_Settings_General extends WMC_Module  {

	public $panel_id;
	

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->panel_id = 'wmr_general';
		$this->register_hook_callbacks();
	}

	public function register_hook_callbacks(){	
		add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab' , 30 );
		add_action( 'woocommerce_settings_tabs_' . $this->panel_id,  __CLASS__. '::settings_tab' );
		add_action( 'woocommerce_update_options_' . $this->panel_id,  __CLASS__. '::save_settings' );		
		add_action( 'woocommerce_settings_' . $this->panel_id,	__CLASS__.'::start_panel' );
		add_action( 'woocommerce_settings_' . $this->panel_id . '_end',	__CLASS__.'::end_panel' );	
		add_action( 'wmc_validation_notices', __CLASS__.'::wmc_validation_error' ); 
        add_action( 'woocommerce_product_data_tabs', __CLASS__. '::wmc_referral_custom_tab',10,1 );
        add_action( 'woocommerce_product_data_panels',  __CLASS__.'::wmc_referral_custom_tab_panel' );
        add_action( 'product_cat_add_form_fields', __CLASS__. '::wmc_add_product_cat_fields' );  
        add_action( 'product_cat_edit_form_fields', __CLASS__. '::wmc_edit_product_cat_fields' );          
        add_action( 'edit_product_cat', __CLASS__. '::wmc_product_cat_fields_save' , 10, 2);  
        add_action( 'create_product_cat', __CLASS__. '::wmc_product_cat_fields_save', 10, 2 );  
       
	}		

	public static function wmc_validation_error($error){
		echo '<div class="wmc_error notice notice-error"><p>'.esc_html($error).'</p></div>';
	}

	public static function start_panel(){
		echo '<div id="wmr_general_setting_panel">';	             
	}	 
	public static function end_panel(){
		echo '</div>';	
	}	 
	/*
	 *	Add setting to 
	 */
	public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['wmr_general'] = __( 'Referral', 'multilevel-referral-affiliate' );
        return $settings_tabs;
    }
	
	public static function settings_tab(){
		woocommerce_admin_fields( self::get_settings() );
	}
	
	/**
	 * Get settings array.
	 *
	 * @return array
	 */
	public static function get_settings() {
        $arrExcludeProducts=get_option('wmc_exclude_products');
        $json_ids    = array();
        if($arrExcludeProducts && is_array($arrExcludeProducts)){
		    $product_ids = array_filter( array_map( 'absint',  get_option('wmc_exclude_products')));
		    foreach ( $product_ids as $product_id ) {
			    $product = wc_get_product( $product_id );
			    if ( is_object( $product ) ) {
				    $json_ids[ $product_id ] = wp_kses_post( html_entity_decode( $product->get_formatted_name(), ENT_QUOTES, get_bloginfo( 'charset' ) ) );
			    }
		    }
        }
        $arrPages=array(0=>__( 'Select Page', 'multilevel-referral-affiliate' ));
        $pages = get_pages(); 
        foreach($pages as $page){
            $arrPages[$page->ID]=__($page->post_title, 'multilevel-referral-affiliate' );
        }
        $ref_=get_option('wmc_sutats');        
        
           $arrSettings=array(
           array( 'title' => esc_html(__( 'Referral Options', 'multilevel-referral-affiliate' )), 'type' => 'title', 'desc' => '', 'id' =>  'wmr_general_setting_panel', 'class' => 'referral_option_title' ),
            array(
                'title'    => esc_html(__( 'Global Store Credit (%)', 'multilevel-referral-affiliate') ),
                'desc'     => '<br>'.esc_html(__( '1. The defined credit points will be deposited in affiliate users account.','multilevel-referral-affiliate')).'<br>'.html_entity_decode(__('2. For more information about "How credit system works?" visit <a href="http://referral.staging.prismitsystems.com/docs/" target="_blank">here</a>', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_store_credit',
                'css'      => 'width: 100px;text-align:right',
                'type'     => 'number',
                'min'     => '0',
                'desc_tip' =>  false
            ),
            array(
                'title'    => __( 'Referral Type', 'wmc' ),
                'desc'     => '',
                'id'       => 'wmc_plan_type',             
                'type'     => 'select',
                'class'    => 'premium_featured',
                'css'      => 'min-width: 100px;',
                'desc_tip' =>  __( 'Regular MLM supports n level child where Binary MLM type only allows two child.', 'wmc' ) ,
                'options'  =>  array()
            ),
            array(
                'title'    => esc_html(__( 'Select Number of levels to distribute credit points.','multilevel-referral-affiliate')),
                'desc'     => '<br>'.esc_html(__( '1. The selected number of levels referrers are entitled to receive credit points.', 'multilevel-referral-affiliate') ).'<br>'.esc_html(__( '2. This setting is only applicable for Recursive Credit System.', 'multilevel-referral-affiliate' )) ,
                'id'       => 'wmc_max_credit_levels',
                'css'      => 'width:100px;',
                'class'    => 'premium_featured', 
                'desc_tip' =>  false,
                'type'     => 'number',
                'value'    => '',
            ),
            array(
                'title'    => esc_html(__( 'Welcome Credit for', 'multilevel-referral-affiliate') ),
                'desc'     => '<br>'.esc_html(__( '1. All Users : All users including the existing ones will be presented with Welcome Credits on their first purchase.', 'multilevel-referral-affiliate') ).'<br>'.esc_html(__('2. New Users : Only the newly registered users will be presented with Welcome Credits on their first purchase. Existing users are not entitled for this benefit.','multilevel-referral-affiliate')).'<br>'.esc_html(__('3. Skip : This option will skip welcome credit for all customers. So customers will not receive welcome credit on their first purchase.','multilevel-referral-affiliate')),
                'id'       => 'wmc_welcome_credit_for',                
                'type'     => 'select',
                'class'    => 'wc-enhanced-select',
                'css'      => 'min-width: 100px;',
                'desc_tip' =>  false,
                'options'  =>  array(
                    'no'   => esc_html(__( 'Skip', 'multilevel-referral-affiliate' )),
                    'all'  => esc_html(__('All Users','multilevel-referral-affiliate')),        
                    'new'  => esc_html(__( 'New Users', 'multilevel-referral-affiliate' ))
                )
            ),
            array(
                'title'    => esc_html(__( 'Welcome Credit (%)', 'multilevel-referral-affiliate') ),
                'desc'     => '<br>'.esc_html(__( 'If Welcome credit is enable for users, then these percentage will be used.','multilevel-referral-affiliate')),
                'id'       => 'wmc_welcome_credit',                
                'type'     => 'number',      
                'class'    => 'premium_featured',
                'value'    => '',            
                'css'      => 'width: 100px;text-align:right;',
                'desc_tip' =>  false
            ),
             
            array(
                'title'    => esc_html(__( 'Credit validity by period', 'multilevel-referral-affiliate') ),
                'desc'     => '<br>'.esc_html(__( 'This sets the number of months/years for expire credits.', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_credit_validity_number',
                'css'      => 'width:100px;',
                'desc_tip' =>  false,
                'type'     => 'number',
                'class'    => 'premium_featured',
                'value'    => '',            
            ),
            
            array(
                'title'    => '',
                'id'       => 'wmc_credit_validity_period',
                'default'  => '',
                'type'     => 'select',
                'class'    => 'wc-enhanced-select set_position',
                'css'      => 'width: 100px;',
                'desc_tip' =>  false,
                'value'    => '', 
                'options'  => array(
                    ''            => esc_html(__( 'Select expiry', 'multilevel-referral-affiliate') ),        
                    'month' => esc_html(__( 'Month','multilevel-referral-affiliate')),   
                    'year'  => esc_html(__( 'Year','multilevel-referral-affiliate')),       
                )
            ),

            array(
                'title'    => esc_html(__( 'Notification Mail Time', 'multilevel-referral-affiliate') ),
                'desc'     => esc_html(__( 'This sets the number of days for send notification mail for expire credits.','multilevel-referral-affiliate')),
                'id'       => 'wmc_notification_mail_time',
                'css'      => 'width:100px;',
                'desc_tip' =>  true,
                'type'     => 'number',
                'class'    => 'premium_featured',
                'value'    => '',            
            ),

            array(
                'title'    => esc_html(__( 'Monthly max credit limit','multilevel-referral-affiliate')).'('.get_woocommerce_currency_symbol().')',
                'desc'     => esc_html(__( 'The credit points will not be credited more than defined limit in the period of one month', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_max_credit_limit',
                'css'      => 'width:100px;',
                'desc_tip' =>  true,
                'type'     => 'number',
                'class'    => 'premium_featured',
                'value'    => '',      
            ),
            
            array(
                'title'    => esc_html(__( 'Max Redemption (%)', 'multilevel-referral-affiliate') ),
                'desc'     => esc_html(__( 'You can define the limit for redemption. If you set 50% then user can not be redeem points more than 50% of product price.', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_max_redumption',
                'css'      => 'width:100px;',
                'desc_tip' =>  true,
                'type'     => 'number',
                'class'    => 'premium_featured',
                'value'    => '100',        
            ),
            
            array(
                'title'    => esc_html(__( 'Exclude products', 'multilevel-referral-affiliate' )),
                'desc'     => esc_html(__( 'Select the product which you want to be exclude from this referral program', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_exclude_products',                
                'css'      => 'width:100%;',
                'desc_tip' =>  true,
                'type'     => 'select',                
                'class'        =>   'premium_featured',                
                'options'   =>  $json_ids,
                'placeholder'    =>  esc_html(__('Exclude products', 'multilevel-referral-affiliate')),
                'custom_attributes'    =>    array(                    
                    'data-action'    =>    'woocommerce_json_search_products',
                    'data-multiple'    =>    'true'
                )
            ),
            array(
                'title'    => esc_html(__( 'Terms And Conditions Page', 'multilevel-referral-affiliate') ),
                'desc'     => esc_html(__( 'Select the terms and condition page', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_terms_and_conditions',                
                'type'     => 'select',
                'class'    => 'wc-enhanced-select',
                'css'      => 'min-width: 100px;',
                'desc_tip' =>  true,
                'options'  =>  $arrPages
            ),
            array(
                'title'    => esc_html(__( 'Auto Join', 'multilevel-referral-affiliate') ),
                'desc'     => esc_html(__( 'Select "Yes" if you want to register users automatically to referral program', 'multilevel-referral-affiliate') ),
                'id'       => 'wmc_auto_register',                
                'type'     => 'select',
                'class'    => 'wc-enhanced-select',
                'css'      => 'min-width: 100px;',
                'desc_tip' =>  true,
                'options'  =>  array(
                    'no'      => esc_html(__( 'No','multilevel-referral-affiliate')),        
                    'yes'     => esc_html(__( 'Yes', 'multilevel-referral-affiliate' ))
                )
            ), 
            array(
                'title'    => esc_html(__( 'Category Credit Preference', 'multilevel-referral-affiliate') ),
                'desc'     => '<br>'.esc_html(__( 'In case of multiple category selected for product, this setting will decide which credit percentage should be used. If "Highest" selected then highest percentage between all the categories will be considered, if "Lowest" selected lowest percentage will be considered', 'multilevel-referral-affiliate' )),
                'id'       => 'wmc_cat_pref',                
                'type'     => 'select',
                'class'    => 'wc-enhanced-select',
                'css'      => 'min-width: 100px;',
                'desc_tip' =>  false,
                'options'  =>  array(
                    'lowest'    => esc_html(__('Lowest','multilevel-referral-affiliate')),        
                    'highest'   => esc_html(__( 'Highest', 'multilevel-referral-affiliate') )
                )
            ),
            );

           $addSettings=apply_filters('wmc_additional_settings',$arrSettings);
           array_push($addSettings, array( 'type' => 'sectionend', 'id' => 'wmr_general_setting_panel'));
           $settings = apply_filters( 'woo_referal_general_settings', $addSettings); 
                
		return apply_filters( 'woocommerce_get_settings_wmc', $settings );
	}
    function wmc_additional_settings($arrSettings){
        $new= array_push($arrSettings,array());
            return $new;
    }
    /* Category add credit input field */
    static function wmc_add_product_cat_fields(){
        global $post;
        ?>
            <div class="form-field">
                <label for="term_meta[wmc_cat_credit]"><?php esc_html_e( 'Global Credit (%)', 'multilevel-referral-affiliate' ); ?></label>
                <input type="number" class="premium_featured" step="0.01" placeholder ="<?php echo get_option('wmc_store_credit');?>" name="term_meta[wmc_cat_credit]" id="term_meta[wmc_cat_credit]" value="">
                <p class="description"><?php esc_html_e( 'Enter a credit percentage, this percentage will apply for all the products in this category','multilevel-referral-affiliate' ); ?></p>
            </div>
        <?php
        $isLevelBaseCredit= get_option('wmc-levelbase-credit',0);
         if($isLevelBaseCredit){
             echo '<div class="form-field"><strong>'.esc_html(__( 'Distribution of commission/credit for each level.','multilevel-referral-affiliate' )).'</strong>';
             $maxLevels=get_option('wmc-max-level',1);
             $maxLevelCredits=get_option('wmc-level-credit',array());             
             $customerCredits=get_option('wmc-level-c',0); 
             echo '<label for="term_meta[wmc_level_c]">'.esc_html(__( 'Customer ', 'multilevel-referral-affiliate' )).' (%)'.'</label><input style="width:50px;text-align:center;" type="number" step="0.01" min="0" max="100" placeholder ="'.$customerCredits.'" name="term_meta[wmc_level_c]" id="term_meta[wmc_level_c]" value="">';             
             for($i=0;$i<$maxLevels;$i++){  
                echo '<label for="term_meta[wmc_level_credit]">'.__( 'Referrer Level ', 'multilevel-referral-affiliate' ).($i+1).' (%)'.'</label><input style="width:50px;text-align:center;" type="number" step="0.01"  min="0" max="100" placeholder ="'.$maxLevelCredits[$i].'" name="term_meta[wmc_level_credit][]" id="term_meta[wmc_level_credit]" value="">';               
             }
             echo '</div>';
         }
    }
    static function wmc_edit_product_cat_fields($term){
        $t_id = $term->term_id;
        $term_meta = get_option( "product_cat_$t_id" );
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[wmc_cat_credit]"><?php esc_html_e( 'Affiliate Credit (%)', 'multilevel-referral-affiliate' ); ?></label></th>
            <td>
                <input style="width:50px;text-align:center;" class="premium_featured" step="0.01"  type="number"  min="0" max="100" placeholder ="<?php echo get_option('wmc_store_credit');?>" name="term_meta[wmc_cat_credit]" id="term_meta[wmc_cat_credit]" value="<?php echo __( $term_meta['wmc_cat_credit'] ) ? __( $term_meta['wmc_cat_credit'] ) : ''; ?>">
                <p class="description"><?php esc_html_e( 'Enter a credit percentage, this percentage will apply for all the products in this category','multilevel-referral-affiliate' ); ?></p>
            </td>
        </tr>
        <?php 
        $isLevelBaseCredit= get_option('wmc-levelbase-credit',0);
        if($isLevelBaseCredit){
            echo '<tr><td colspan="2"><Strong>'.esc_html(__( 'Distribution of commission/credit for each level','multilevel-referral-affiliate' )).'</Strong></td>';
            $maxLevels=get_option('wmc-max-level',1);   
            $maxLevelCredits=get_option('wmc-level-credit',array()); 
            $customerCredits=get_option('wmc-level-c',0); 
            $Cvalue=(isset($term_meta['wmc_level_c']) && $term_meta['wmc_level_c']!='')?$term_meta['wmc_level_c']:''; ?>
             <tr class="form-field">
                    <th scope="row" valign="top"><label for="wmc_level_c"><?php echo esc_html(__( 'Customer ', 'multilevel-referral-affiliate' )); ?></label></th>
                    <td>
                        <input style="width:50px;text-align:center;" step=""0.01 type="number" min="0" max="100" placeholder ="<?php echo $customerCredits;?>" name="term_meta[wmc_level_c]" id="wmc_level_c" value="<?php echo esc_html($Cvalue); ?>">
                       <span>(%)</span> 
                    </td>
                </tr> 
            <?php           
             for($i=0;$i<$maxLevels;$i++){ 
                 $Lvalue=(isset($term_meta['wmc_level_credit'][$i]) && $term_meta['wmc_level_credit'][$i]!='')?$term_meta['wmc_level_credit'][$i]:'';
        ?>
                <tr class="form-field">
                    <th scope="row" valign="top"><label for="wmc_level_credit_<?php echo $i;?>"><?php echo esc_html(__( 'Referrer Level ', 'multilevel-referral-affiliate') ).($i+1); ?></label></th>
                    <td>
                        <input style="width:50px;text-align:center;" step="0.01" type="number" min="0" max="100" placeholder ="<?php echo $maxLevelCredits[$i];?>" name="term_meta[wmc_level_credit][]" id="wmc_level_credit_<?php echo $i;?>" value="<?php echo esc_html($Lvalue); ?>">
                        <span>(%)</span> 
                    </td>
                </tr>             
        <?php
             }
        }
    }
    static function wmc_product_cat_fields_save($term_id){
        if ( isset( $_POST['term_meta'] ) ) {         
            $t_id = $term_id;
            $term_meta = get_option( "product_cat_$t_id" );
            $cat_keys = array_keys( wmc_custom_sanitize_array($_POST['term_meta']) );
            foreach ( $cat_keys as $key ) {
                if ( isset ( $_POST['term_meta'][$key] ) ) {
                    if($key=='wmc_cat_credit'){
                        $_POST['term_meta'][$key]=floatval(wmc_custom_sanitize_array($_POST['term_meta'][$key]));                    
                        }
                    $term_meta[$key] = wmc_custom_sanitize_array($_POST['term_meta'][$key]);
                }
            }
            
            // Save the option array.
            update_option( "product_cat_$t_id", $term_meta );
        }
    }
    /* end */ 

	/**
	 * Save settings.
	 */
	public static function save_settings() {        
        if(isset($_POST['wmc_paytm_merchant_key']) && !empty($_POST['wmc_paytm_merchant_key'])){
            update_option('wmc_paytm_merchant_key',sanitize_text_field($_POST['wmc_paytm_merchant_key']));
            unset($_POST['wmc_paytm_merchant_key']);
        }
        
        woocommerce_update_options(self::get_settings());
	}
    static function wmc_add_custom_admin_product_tab() {
    ?>
        <li class="referral_tab"><a href="#referral_tab_data"><?php esc_html_e('Multilevel Referral', 'multilevel-referral-affiliate'); ?></a></li>
    <?php
    }
	static function wmc_add_custom_general_fields(){
        global $woocommerce, $post;
        echo '<div class="options_group"><h4 style="padding-left:10px;">'.esc_html(__('Multilevel Referral Plugin Settings','multilevel-referral-affiliate')).'</h4>';
        woocommerce_wp_text_input( 
            array( 
                'id'          => 'wmc_credits', 
                'label'       => esc_html(__( 'Affiliate Credit (%)', 'multilevel-referral-affiliate') ), 
                'placeholder' => get_option('wmc_store_credit'),
                'desc_tip'    => true,
                'description' => esc_html(__( '1. The defined credit points will be deposited in affiliate users account, when user purchase this product.','multilevel-referral-affiliate')).'<br>'.html_entity_decode(__('2. For more information about "How credit system works?" visit','multilevel-referral-affiliate')).'<a href="http://referral.staging.prismitsystems.com/shop/" target="_blank">'.esc_html(__('here','multilevel-referral-affiliate')).'</a>'
            )
        );
        echo '</div>';
    }
    static function wmc_referral_custom_tab($default_tabs){
         $default_tabs['wmc_referral_tab'] = array(
            'label'   =>  esc_html(__( 'Referral', 'multilevel-referral-affiliate') ),
            'target'  =>  'wmc_referral_custom_tab_panel',
            'priority' => 60,
            'class'   => array()
        );
        return $default_tabs;
    }
    static function wmc_referral_custom_tab_panel(){
        global $woocommerce, $post;
         echo '<div id="wmc_referral_custom_tab_panel" class="panel woocommerce_options_panel">
         <h4 style="padding-left:10px;">'.esc_html(__('Multilevel Referral Plugin Settings','multilevel-referral-affiliate')).'</h4>
         <div class="options_group">'; 
         woocommerce_wp_text_input( 
            array( 
                'id'          => 'wmc_credits', 
                'label'       => esc_html(__( 'Global Credit (%)', 'multilevel-referral-affiliate') ), 
                'type'          => 'number',
                'class'         => 'premium_featured', 
                'style'          => 'width:50px;text-align:right;', 
                'placeholder' => get_option('wmc_store_credit'),
                'desc_tip'    => true,
                'description' => esc_html(__( '1. The defined credit points will be deposited in affiliate users account, when user purchase this product.','multilevel-referral-affiliate')).'<br>'.html_entity_decode(__('2. For more information about "How credit system works?" visit','multilevel-referral-affiliate')).' <a href="http://referral.staging.prismitsystems.com/shop/" target="_blank">'.esc_html(__('here', 'multilevel-referral-affiliate') ).'</a>' 
            )
        );
         echo '</div>';
         $isLevelBaseCredit= get_option('wmc-levelbase-credit',0);
         if($isLevelBaseCredit){
             echo '<div class="options_group"><h4 style="padding-left:10px;">'.esc_html(__('Distribution of commission/Credit for each level.','multilevel-referral-affiliate')).'</h4>'; 
             $maxLevels=get_option('wmc-max-level',1);
             $maxLevelCredits=get_option('wmc-level-credit',array());
             $customerCredits=get_option('wmc-level-c',0);
             $maxProductLevelCredits=get_post_meta($post->ID,'wmc-level-credit',true);
             $CCredits=get_post_meta($post->ID,'wmc-level-c',true);
             echo '<p class="form-field wmc-level-c_field">
		<label for="wmc-level-c">'.esc_html(__('Customer (%)','multilevel-referral-affiliate')).'</label><input type="number" step="0.01" class="short" style="width:50px;text-align:right;" name="wmc-level-c" id="wmc-level-c" value="'.$CCredits.'" placeholder="'.$customerCredits.'"> </p>';
             for($i=0;$i<$maxLevels;$i++){
                 $levelValue=(isset($maxProductLevelCredits[$i]) && $maxProductLevelCredits[$i]!='')?$maxProductLevelCredits[$i]:'';
                 woocommerce_wp_text_input( 
                    array( 
                        'id'          => 'wmc-level-credit', 
                        'name'          => 'wmc-level-credit[]', 
                        'type'          => 'number', 
                        'min'          => 0, 
                        'style'          => 'width:50px;text-align:right;', 
                        'label'       => esc_html(__( 'Referrer Level ', 'multilevel-referral-affiliate') ).($i+1).' (%)', 
                        'placeholder' => $maxLevelCredits[$i],
                        'desc_tip'    => false,
                        'value' => $levelValue
                    )
                );
             }
             echo '</div>';
         }
         echo '</div>';
    }
	public function activate( $network_wide ){
		
	}

	/**
	 * Rolls back activation procedures when de-activating the plugin
	 *
	 * @mvc Controller
	 */
	public function deactivate(){
		
	}

	/**
	 * Initializes variables
	 *
	 * @mvc Controller
	 */
	public function init(){
		
	}

	/**
	 * Checks if the plugin was recently updated and upgrades if necessary
	 *
	 * @mvc Controller
	 *
	 * @param string $db_version
	 */
	public function upgrade( $db_version = 0 ){
		
	}

	/**
	 * Checks that the object is in a correct state
	 *
	 * @mvc Model
	 *
	 * @param string $property An individual property to check, or 'all' to check all of them
	 * @return bool
	 */
	public function is_valid($valid = "all"){
		return true;
	}
    
}
endif;