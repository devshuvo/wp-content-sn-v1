<?php
/**
 * WooCommerce Product Table Addon
 *
 * @since 1.0.0
 */
class SPLENDID_DISCOVER_CLASS extends \Elementor\Widget_Base {


	public function get_name() {
		return 'discover_splendid';
	}


	public function get_title() {
		return __( 'Discover Splendid', 'wpte-addon' );
	}

	public function get_icon() {
		return 'eicon-slides';
	}

	public function get_categories() {
		return [ 'splendid' ];
	}

	public function get_keywords() {
		return [ 'hero', 'slider', 'splendid', 'discover', 'sn' ];
	}

    public function get_script_depends() {
        return [ 'splendid-plugin' ];
    }

    public function get_style_depends() {
        return [ 'splendid-plugin' ];
    }


	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'wpte-addon' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'heading',
			[
				'label' => __( 'Title', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Discover Splendid', 'splendid' ),
				'label_block' => true,
			]
		);

        $this->end_controls_section();

	}

	protected function render() {

		global $post, $product, $woocommerce;

		$settings = $this->get_settings_for_display();

		$heading = $settings['heading'];

		if( $heading ) :
		?>
		<div class="sn-discover-wrap doparallax">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/discover_shape_left.svg" class="discover_shape_left">

			<img src="<?php echo SPLENDID_IMG_DIR; ?>/discover_shape_right.svg" class="discover_shape_right" data-stellar-ratio="2">

			<div class="ast-container">
				<div class="sn-discover-inner text-center">
					<h3><?php _e( $heading ); ?></h3>
				</div>
			</div>
		</div>
		<?php
		endif;

	}

}