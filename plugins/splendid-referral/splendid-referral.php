<?php
/**
 * Plugin Name: Splendid Referral
 * Description: Custom Referral Plugin for Splendid
 * Plugin URI:  https://www.splendidnutrition.com/
 * Version:     1.0.0
 * Author:      Shuvo
 * Author URI:  https://www.splendidnutrition.com/
 * Text Domain: splendid
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! defined( 'SPLENDID_REF' ) ) {
	define( 'SPLENDID_REF', true );
}

if ( ! defined( 'SPLENDID_REF_FREE' ) ) {
	define( 'SPLENDID_REF_FREE', true );
}

if ( ! defined( 'SPLENDID_REF_URL' ) ) {
	define( 'SPLENDID_REF_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'SPLENDID_REF_DIR' ) ) {
	define( 'SPLENDID_REF_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'SPLENDID_REF_INC' ) ) {
	define( 'SPLENDID_REF_INC', SPLENDID_REF_DIR . 'includes/' );
}

if ( ! defined( 'SPLENDID_REF_INIT' ) ) {
	define( 'SPLENDID_REF_INIT', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'SPLENDID_REF_FREE_INIT' ) ) {
	define( 'SPLENDID_REF_FREE_INIT', plugin_basename( __FILE__ ) );
}


final class SplendidReferral {

	// Instance
	private static $_instance = null;

	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	public function __construct() {

		add_action( 'plugins_loaded', [ $this, 'init' ] );

	}

	public function init() {
		add_filter( 'woocommerce_account_menu_items', array( $this, 'add_my_account_menu' ), 15 );
		add_action( 'init', array( $this, 'custom_endpoints' ) );

		// so you can use is_wc_endpoint_url( 'refunds-returns' )
		add_filter( 'woocommerce_get_query_vars', array( $this, 'custom_woocommerce_query_vars' ), 0 );

		add_action( 'woocommerce_account_refer-friend_endpoint', array( $this, 'refer_friend_endpoint_content' ) );
		add_action( 'woocommerce_account_my-rewards_endpoint', array( $this, 'my_rewards_endpoint_content' ) );

	}


    public function add_my_account_menu($items) {

        $key = array_search('orders', array_keys($items));

        if($key !== false){

            $items = (array_merge(array_splice($items, 0, $key + 1), array('refer-friend' => __('Refer-A-Friend','splendid')), $items));
            $items = (array_merge(array_splice($items, 0, $key + 2), array('my-rewards' => __('My Rewards','splendid')), $items));

        }

        else{

            $items['refer-friend'] = __('Refer-A-Friend','splendid');
            $items['my-rewards'] = __('My Rewards','splendid');

        }

        return $items;
    }

	function custom_endpoints() {
	    add_rewrite_endpoint( 'refer-friend', EP_ROOT | EP_PAGES );
	    add_rewrite_endpoint( 'my-rewards', EP_ROOT | EP_PAGES );
	}

	function custom_woocommerce_query_vars( $vars ) {
		$vars['refer-friend'] = 'refer-friend';
		$vars['my-rewards'] = 'my-rewards';
		return $vars;
	}

	function refer_friend_endpoint_content() {
	    wc_get_template( 'myaccount/refer-friend.php');
	}

	function my_rewards_endpoint_content() {
	    wc_get_template( 'myaccount/my-rewards.php');
	}


}

SplendidReferral::instance();


//require_once( SPLENDID_REF_INC . 'class.yith-wcaf-affiliate-handler.php' );

/**
 * Encrypt and decrypt
 *
 * @param string $string string to be encrypted/decrypted
 * @param string $action what to do with this? e for encrypt, d for decrypt
 */
function sn_simple_crypt( $string, $action = 'e' ) {
    // you may change these values to your own
    $secret_key = 'my_simple_secret_key';
    $secret_iv = 'my_simple_secret_iv';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }

    return $output;
}

// Encrypt
function sn_encrypt( $data = null ){
	return sn_simple_crypt( $data, 'e' );
}

// Decrypt
function sn_decrypt( $data = null ){
	return sn_simple_crypt( $data, 'd' );
}

// Not required Payment mail
add_filter('yith_wcaf_payment_email_required', '__return_false');

function is_affiliate_enabled(){
	if ( is_user_logged_in() ) {
		$customer_id = get_current_user_id();
		$affiliates  = YITH_WCAF_Affiliate_Handler()->get_affiliates( array( 'user_id' => $customer_id ) );
		$affiliate   = isset( $affiliates[0] ) ? $affiliates[0] : 0;

		return ( $affiliate['enabled'] == 1 ) ? 1 : 0;

	} else {
		return;
	}
}

/**
 * /friends redirect to ref
 */
function sn_friends_url_redirect() {


    if ( is_affiliate_enabled() && is_page( 'friends' ) ) {

	    wp_redirect( wc_get_account_endpoint_url('refer-friend') );

        die;
    }

}
add_action( 'template_redirect', 'sn_friends_url_redirect' );

function yith_wcaf_new_affiliate_enable_auto( $affiliate_id ){
	if ( !is_affiliate_enabled() ) {
		$res = YITH_WCAF_Affiliate_Handler()->update( $affiliate_id, array( 'enabled' => 1 ) );
	} else {
		return;
	}

}
add_action( 'yith_wcaf_new_affiliate', 'yith_wcaf_new_affiliate_enable_auto' );


/**
 * Auto enable affilate
 */
function enable_auto_affiliate(){

	if ( is_user_logged_in() ) {
		$customer_id = get_current_user_id();
		$affiliates  = YITH_WCAF_Affiliate_Handler()->get_affiliates( array( 'user_id' => $customer_id ) );
		$affiliate   = isset( $affiliates[0] ) ? $affiliates[0] : false;

		if ( ! $affiliate ) {
			$validation_error = new WP_Error();
			$validation_error = apply_filters( 'yith_wcaf_process_become_an_affiliate_errors', $validation_error, $customer_id );

			if ( $validation_error->get_error_code() ) {
				wc_add_notice( $validation_error->get_error_message(), 'error' );
			} else {
				$id = YITH_WCAF_Affiliate_Handler()->add( array(
					'user_id' => $customer_id,
					'enabled' => true,
					'token'   => YITH_WCAF_Affiliate_Handler()->get_default_user_token( $customer_id )
				) );

				if ( $id ) {

					//wc_add_notice( __( 'Your request has been processed correctly', 'yith-woocommerce-affiliates' ) );

					// trigger new affiliate action
					do_action( 'yith_wcaf_new_affiliate', $id );
				} else {
					//wc_add_notice( __( 'An error occurred while trying to create the affiliate; try later.', 'yith-woocommerce-affiliates' ), 'error' );
				}
			}
		} else {
			//wc_add_notice( __( 'You have already affiliated with us!', 'yith-woocommerce-affiliates' ), 'error' );
		}
	}

}
//add_action( 'wp_loaded', 'enable_auto_affiliate' );

/**
 * Referal Landing Page redirect
 */
function sn_referral_landing_redirect() {

	$ref = get_option( 'yith_wcaf_referral_var_name', true );

    if ( isset( $_GET[$ref] ) && is_front_page() ) {
        wp_redirect( home_url( '/boxdeal/?'.$ref.'='.$_GET[$ref] ) );
        die;
    }

}
add_action( 'template_redirect', 'sn_referral_landing_redirect' );

/**
 * Referal Landing Page redirect
 */
function sn_get_rewards(){
	if ( is_user_logged_in() ) {
		$customer_id = get_current_user_id();
		$affiliates  = YITH_WCAF_Affiliate_Handler()->get_affiliates( array( 'user_id' => $customer_id ) );
		$affiliate   = isset( $affiliates[0] ) ? $affiliates[0] : 0;

		return ( $affiliate['conversion'] ) ? $affiliate['conversion'] : 0;

	} else {
		return;
	}
}


/**
 * Set Cookie Data
 */
function sn_setcookie( $cookie = null, $value = null ){

    $expiry = strtotime('+1 day');

	if ( $cookie && $value ) {
	    setcookie($cookie, $value, $expiry, COOKIEPATH, COOKIE_DOMAIN);
	    return true;
	} else {
		return false;
	}
}

/**
 * Get Cookie Data
 */
function sn_get_cookie( $cookie = null ){
	if ( $cookie && isset( $_COOKIE[$cookie] ) ) {
		return $_COOKIE[$cookie];
	} else {
		return false;
	}
}

/**
 * Sitewide Set Cookie Function
 *
 */
function splendid_setcookie_sitewide(){
    $user_id = get_current_user_id();

    $expiry = strtotime('+12 month');

    if( isset( $_POST['action'] ) && isset( $_POST['EMAIL'] ) && $_POST['action'] == "claim_my_offer_box" ) {

    	if ( !is_user_logged_in() ) {
    		sn_setcookie( 'referrer_email', $_POST['EMAIL'] );
        	sn_setcookie( 'new_referrer', sn_encrypt('1') );
        	//setcookie('referrer_created_account', '1', $expiry, $path, $host);
    	}
    }
}
add_action('init', 'splendid_setcookie_sitewide', 5 );
//add_action('template_redirect', 'splendid_setcookie_sitewide', 5 );

/**
 * Get Cookie Data
 */
function sn_delete_cookie( $cookie = null ){

    $expiry = strtotime('-1 day');

	if ( $cookie && isset( $_COOKIE[$cookie] ) ) {
	    unset( $_COOKIE[$cookie] );

	    setcookie($cookie, '', $expiry, COOKIEPATH, COOKIE_DOMAIN);

	    return true;
	} else {
		return false;
	}
}

/**
 * Delete Cookies On logout
 */
function delete_cookies_on_logout_function() {
    // your code
}
add_action( 'wp_logout', 'delete_cookies_on_logout_function' );

/**
 * Get referrer email
 */
function sn_get_referrer_email(){
	return sn_get_cookie('referrer_email');
}

/**
 * Add a discount to an Orders programmatically
 * (Using the FEE API - A negative fee)
 *
 * @since  3.2.0
 * @param  int     $order_id  The order ID. Required.
 * @param  string  $title  The label name for the discount. Required.
 * @param  mixed   $amount  Fixed amount (float) or percentage based on the subtotal. Required.
 * @param  string  $tax_class  The tax Class. '' by default. Optional.
 */
function wc_order_add_discount( $order_id, $title, $amount, $tax_class = '' ) {
    $order    = wc_get_order($order_id);
    $subtotal = $order->get_subtotal();
    $item     = new WC_Order_Item_Fee();

    if ( strpos($amount, '%') !== false ) {
        $percentage = (float) str_replace( array('%', ' '), array('', ''), $amount );
        $percentage = $percentage > 100 ? -100 : -$percentage;
        $discount   = $percentage * $subtotal / 100;
    } else {
        $discount = (float) str_replace( ' ', '', $amount );
        $discount = $discount > $subtotal ? -$subtotal : -$discount;
    }

    $item->set_tax_class( $tax_class );
    $item->set_name( $title );
    $item->set_amount( $discount );
    $item->set_total( $discount );

    if ( '0' !== $item->get_tax_class() && 'taxable' === $item->get_tax_status() && wc_tax_enabled() ) {
        $tax_for   = array(
            'country'   => $order->get_shipping_country(),
            'state'     => $order->get_shipping_state(),
            'postcode'  => $order->get_shipping_postcode(),
            'city'      => $order->get_shipping_city(),
            'tax_class' => $item->get_tax_class(),
        );
        $tax_rates = WC_Tax::find_rates( $tax_for );
        $taxes     = WC_Tax::calc_tax( $item->get_total(), $tax_rates, false );
        print_pr($taxes);

        if ( method_exists( $item, 'get_subtotal' ) ) {
            $subtotal_taxes = WC_Tax::calc_tax( $item->get_subtotal(), $tax_rates, false );
            $item->set_taxes( array( 'total' => $taxes, 'subtotal' => $subtotal_taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        } else {
            $item->set_taxes( array( 'total' => $taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        }
        $has_taxes = true;
    } else {
        $item->set_taxes( false );
        $has_taxes = false;
    }
    $item->save();

    $order->add_item( $item );
    $order->calculate_totals( $has_taxes );
    $order->save();
}


/**
 * Custom price function
 */
function wc_formatted_price( $price, $args = array() ) {
    extract( apply_filters( 'wc_price_args', wp_parse_args( $args, array(
        'ex_tax_label' => false,
        'currency' => '',
        'decimal_separator' => wc_get_price_decimal_separator(),
        'thousand_separator' => wc_get_price_thousand_separator(),
        'decimals' => wc_get_price_decimals(),
        'price_format' => get_woocommerce_price_format(),
 	) ) ) );

    $negative = $price < 0;
    $price = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
    $price = apply_filters( 'formatted_woocommerce_price', number_format( $price, $decimals, $decimal_separator, $thousand_separator ), $price, $decimals, $decimal_separator, $thousand_separator );

    if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $decimals > 0 ) {
        $price = wc_trim_zeros( $price );
    }

    $formatted_price = ( $negative ? '-' : '' ) . sprintf( $price_format, get_woocommerce_currency_symbol( $currency ) , $price );


    return $formatted_price;
}

function sn_new_referrer_has_discount(){

	if ( sn_decrypt(sn_get_cookie('new_referrer')) == 1 ) {
		return true;
	} elseif ( sn_decrypt(sn_get_cookie('is_user_done_survey')) == 1 ) {
		return true;
	}

	if ( is_user_logged_in() ) {

		$user_id = get_current_user_id();

		if ( get_user_meta( $user_id, 'new_referrer', true ) == 1 ) {
			return true;
		}  elseif( sn_get_rewards() > 3 ){
			return true;
		}

	}

	return false;
}


/**
 * Apply Discount for referrer
 */
add_action( 'woocommerce_cart_calculate_fees', 'sn_referrer_custom_discount', 10, 1 );
function sn_referrer_custom_discount( $cart ){
    if ( is_admin() && ! defined( 'DOING_AJAX' ) ){
        return;
    }

    // Only for 2 items or more
    if( sn_new_referrer_has_discount() ){
        //$discount = WC()->cart->get_subtotal() * $percentage / 100;
        $discount = 5;

        // Apply discount to 2nd item for non on sale items in cart
        if( $discount > 0 ){
            $cart->add_fee( sprintf( __("Discount (%s off available)"), wc_formatted_price($discount)), -$discount );
        }
    }
}

/**
 * Check user free shipping availability
 *
 */
function sn_user_has_free_shipping(){
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();
		if ( get_user_meta( $user_id, 'has_free_shipping', true ) > 0 ) {
			return true;
		}
	}

	return false;
}

/**
 * Clear Shipping Rates Cache
 */
add_filter('woocommerce_checkout_update_order_review', 'clear_wc_shipping_rates_cache');
function clear_wc_shipping_rates_cache(){
    $packages = WC()->cart->get_shipping_packages();

    foreach ($packages as $key => $value) {
        $shipping_session = "shipping_for_package_$key";

        unset(WC()->session->$shipping_session);
    }
}

/**
 * Free Shipping
 */
add_filter( 'woocommerce_package_rates', 'free_shipping_for_referrer', 20, 2 );
function free_shipping_for_referrer( $rates, $package ) {
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ){
        return;
    }

	if( sn_user_has_free_shipping() ) {

	    foreach( $rates as $rate_key => $rate ){
	        // Excluding free shipping methods
	        if( $rate->method_id != 'free_shipping'){
	            // Set cost to zero
        	    //$rates[$rate_key]->method_id = 'free_shipping';
        	    $rates[$rate_key]->cost = 0;
        	    $rates[$rate_key]->label = 'Free';
	        }
	    }
    }

    return $rates;
}

/**
 * More to count - Referral Page
 * /my-account/refer-friend/
 */
function sn_more_to_go_count(){
	$need_steps = 3;
	if ( sn_get_rewards() < $need_steps ) {
		$return = $need_steps - sn_get_rewards();
	} else {
		$return = 0;
	}

	return  $return;
}


/**
 * More to count with text - Referral Page
 * /my-account/refer-friend/
 */
function sn_more_to_go_count_with_text(){

	$ref_count = sn_more_to_go_count();

	if ( $ref_count == 0 ) {

		return "Complete";

	} else {
		return "Progress toward a FREE Box! {$ref_count} more to go!";
	}

	return;
}



/**
 * Steps - Referral Page
 * /my-account/refer-friend/
 */
function sn_referrer_progress_count( $count = null ){
	if ( $count ) {
		$classes = "";

		if ( sn_get_rewards() >= $count ) {
			$classes = " active ";
		}

		return "<li class='{$classes}'>{$count}</li>";
	}

	return;
}

/**
 * Social Share Tags
 */
function sn_opengraph_share_tags(){
	$ref = get_option( 'yith_wcaf_referral_var_name', true );

	if ( isset( $_GET[$ref] ) ) :
		//$share_title           = apply_filters( 'yith_wcaf_socials_share_title', __( 'Share on:', 'yith-woocommerce-affiliates' ) );
		//$share_link_url        = $generated_url;
		//$share_links_title     = apply_filters( 'plugin_text', urlencode( get_option( 'yith_wcaf_socials_title' ) ) );
		//$share_twitter_summary = urlencode( str_replace( '%referral_url%', '', get_option( 'yith_wcaf_socials_text' ) ) );
		//$share_summary         = urlencode( str_replace( '%referral_url%', $share_link_url, get_option( 'yith_wcaf_socials_text' ) ) );
		//$share_image_url       = urlencode( get_option( 'yith_wcaf_socials_image_url' ) );

		$og_url = home_url( '/?'.$ref.'='.$_GET[$ref] );
		$og_type = 'website';
		$og_title = apply_filters( 'plugin_text', get_option( 'yith_wcaf_socials_title' ) );
		$og_description =  str_replace( '%referral_url%', $og_url, get_option( 'yith_wcaf_socials_text' ) );
		$og_image = esc_url( get_option( 'yith_wcaf_socials_image_url' ) );
		//$og_image = "https://www.splendidnutrition.com/wp-content/uploads/2020/09/Hero-1.png";

    ?>
		<meta property="og:url"           content="<?php echo $og_url; ?>" />
		<meta property="og:type"          content="<?php echo $og_type; ?>" />
		<meta property="og:title"         content="<?php echo $og_title; ?>" />
		<meta property="og:description"   content="<?php echo $og_description; ?>" />
		<meta property="og:image"         content="<?php echo $og_image; ?>" />
	<?php endif; ?>

	<?php
}
add_action( 'wp_head', 'sn_opengraph_share_tags', 5 );