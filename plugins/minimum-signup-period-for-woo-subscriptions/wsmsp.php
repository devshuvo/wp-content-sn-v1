<?php
/**
* Plugin Name: Minimum Signup Period For WooCommerce Subscriptions
* Plugin URI: https://www.elliotvs.co.uk/plugins/minimum-signup-period-for-woocommerce-subscriptions
* Description: Allows you to create a minimum signup period for WooCommerce subscriptions. The customer will pay for the full initial minimum period upfront, then after that period ends, the subscription will renew as normal each month. Doesn't use "trial" period and works with product addons etc.
* Version: 1.0.1
* Author: ElliotVS
* Author URI: https://www.elliotvs.co.uk
* License: GPL12
*
* WC requires at least: 3.4
* WC tested up to: 4.0.0
**/

include plugin_dir_path( __FILE__ ) . 'admin-options.php';

// Redirect to settings on activate
register_activation_hook( __FILE__, 'wsmsp_my_plugin_activate' );
add_action( 'admin_init', 'wsmsp_my_plugin_redirect' );
function wsmsp_my_plugin_activate()
{
	add_option( 'wsmsp_my_plugin_do_activation_redirect', true );
}

function wsmsp_my_plugin_redirect()
{
	
	if ( get_option( 'wsmsp_my_plugin_do_activation_redirect', false ) ) {
		delete_option( 'wsmsp_my_plugin_do_activation_redirect' );
		wp_redirect( "/wp-admin/admin.php?page=wsmsp" );
	}

}

// CHECK IF MONTHLY
function wsmsp_is_cart_monthly() {
    foreach ( WC()->cart->get_cart() as $cart_item ) {
        $product = $cart_item['data'];
        if(!empty($product)){
            // $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'single-post-thumbnail' );
			$productitle = $product->get_title();
			if (strpos($productitle, 'Monthly') !== false) {
				return true;
			}
        }
    }
	return false;
}

// CHANGE CART PRICE
add_action( 'woocommerce_before_calculate_totals', 'wsmsp_modify_cart_price', 20, 1);
function wsmsp_modify_cart_price( $cart_obj ) {

$options = get_option( 'wsmsp_options' );
if($options['wsmsp_field_number_months']) { $numbermonths = $options['wsmsp_field_number_months']; } else { $numbermonths = 1; }

if(wsmsp_is_cart_monthly()) {

    if ( is_admin() && ! defined( 'DOING_AJAX' ) || ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 ))
        return;

    foreach ( $cart_obj->get_cart() as $cart_item ) {
        $cart_item['data']->set_price( $cart_item['data']->get_price() * $numbermonths );
    }

}
}

// Adds Custom Text
add_action( 'woocommerce_review_order_before_payment', 'wsmsp_modify_cart_text_after');

function wsmsp_modify_cart_text_after( $cart_obj ) {
if(wsmsp_is_cart_monthly()) {
	
$options = get_option( 'wsmsp_options' );

if($options['wsmsp_field_text']) {
$option_text = $options['wsmsp_field_text'];
} else {
$option_text = "The first {number} months will be billed upfront today.
<br/><br/>
You will then be billed the 'recurring total' every month starting on the date: {date}";
}

if($options['wsmsp_field_number_months']) { $numbermonths = $options['wsmsp_field_number_months']; } else { $numbermonths = 1; }

$date = date("jS \of F Y", strtotime(" +".$numbermonths." months"));
$option_text = str_replace("{number}", $numbermonths, $option_text);
$option_text = str_replace("{date}", $date, $option_text);
	
?>
<style>.first-payment-date { display: none; }</style>

<?php

    echo "<div style='border: 4px solid #f3f3f3;padding: 10px;'><strong style='color: green;'>"
	. $option_text .
	"</strong></div><br/><br/>";

}
}

// Adds Custom Text
add_action( 'woocommerce_review_order_after_cart_contents', 'wsmsp_modify_cart_text_total');

function wsmsp_modify_cart_text_total( $cart_obj ) {
	
$options = get_option( 'wsmsp_options' );

if($options['wsmsp_field_text2']) {
$option_text2 = $options['wsmsp_field_text2'];
} else {
$option_text2 = "for the first {number} months.";
}

if($options['wsmsp_field_number_months']) { $numbermonths = $options['wsmsp_field_number_months']; } else { $numbermonths = 1; }

$date = date("jS \of F Y", strtotime(" +".$numbermonths." months"));
$option_text2 = str_replace("{number}", $numbermonths, $option_text2);
$option_text2 = str_replace("{date}", $date, $option_text2);

if(wsmsp_is_cart_monthly()) {
?>
<style>
.cart-subtotal:not(.recurring-total) span.amount::after {
	content: " <?php echo $option_text2; ?>";
}
</style>
<?php
}
}

// Thank You Page - Update Renewal Date

add_action('woocommerce_checkout_subscription_created', 'wsmsp_next_payment_date_change', 10, 3);  

function wsmsp_next_payment_date_change( $subscription, $order, $recurring_cart ){
	
	$options = get_option( 'wsmsp_options' );
	$numbermonths = $options['wsmsp_field_number_months'];
				
	$subid = $subscription->get_id();
	$items = $order->get_items();

	foreach ( $items as $item ) {
		
		$productitle = $item['name'];
		
		if(strpos($productitle, 'Monthly') !== false) {

				$nextdate = get_post_meta( $subid, '_schedule_next_payment', true );
				$newdate = date( 'Y-m-d H:i:s', strtotime( '+'.$numbermonths.' month', strtotime( $nextdate )) );
				update_post_meta( $subid , '_schedule_next_payment', $newdate);

		}
		
	}
	
}